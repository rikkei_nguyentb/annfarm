﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace BUS
{
    public interface IStatistical
    {
        BindingList<GetStaInfoDto> GetStatisticalDic(StatisticalDto dto);
    }
}