﻿using System.Collections.Generic;
using DAO.Model;
using DTO.FormView;

namespace BUS
{
    public interface IUtility
    {
        CustomerModel GetCustomer(string tel);
        IEnumerable<CustomerModel> GetCustomer();
        IEnumerable<ProductModel> GetProduct();

        /// <summary>
        /// if null, return new obj
        /// </summary>
        /// <param name="productID"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        DiscountModel GetDiscount(string productID, string quantity);

        bool UpdateCustomer(MstCustomerView vm);
    }
}
