﻿using System;
using System.Globalization;

namespace BUS
{
    public static class Utility
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertToCurrency(object value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            return string.Format("{0:0,0}", Convert.ToDecimal(value));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNumeric(object value)
        {
            if (value == null)
            {
                return true;
            }

            return decimal.TryParse(value.ToString(), System.Globalization.NumberStyles.Currency, CultureInfo.InvariantCulture, out decimal result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal ConvertToDec(object value)
        {
            decimal result;

            if (value == null)
            {
                return 0;
            }

            decimal.TryParse(value.ToString(), NumberStyles.Currency, CultureInfo.InvariantCulture, out result);

            return result;
        }
    }
}
