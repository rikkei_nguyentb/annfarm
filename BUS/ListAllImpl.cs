﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO;

namespace BUS
{
    public class ListAllImpl : IListAll
    {
        private IListAllRepository dao;

        public ListAllImpl(IListAllRepository _dao)
        {
            dao = _dao;
        }

        public BindingList<GridDTO> GetListAll(ListAllDTO dto)
        {
            BindingList<GridDTO> tmp = dao.GetListAll(dto.Tel, dto.FullName, dto.ShipDateFrom, dto.ShipDateTo, dto.Product, dto.Com, dto.Incom);
            BindingList<GridDTO> result = new BindingList<GridDTO>();

            int count = 1;
            string OrderIdOld = string.Empty;
            GridDTO d = new GridDTO();

            foreach (GridDTO i in tmp)
            {

                if (!string.Equals(OrderIdOld, i.OrderID) || string.IsNullOrEmpty(OrderIdOld))
                {
                    if (!string.IsNullOrEmpty(OrderIdOld)
                        && ((string.Equals(dto.Incom, "Checked") && !string.Equals(d.Total, d.Payed))
                            || (string.Equals(dto.Com, "Checked") && string.Equals(d.Total, d.Payed))
                            || (string.Equals(dto.Com, "Unchecked") && string.Equals(dto.Incom, "Unchecked"))))
                    {
                        d.No = count++.ToString();
                        result.Add(d);
                    }                    

                    d = new GridDTO();

                    d.FullName = i.FullName;
                    d.FeeShip = string.IsNullOrEmpty(i.FeeShip)? "0" : i.FeeShip;
                    d.Payed = i.Payed;
                    d.Product = i.Product;
                    d.Quantity = i.Quantity;
                    d.Remark = i.Remark;
                    d.Total = Utility.ConvertToCurrency(Utility.ConvertToDec(i.FeeShip) + Utility.ConvertToDec(i.Total));
                    d.OrderID = i.OrderID;
                    d.ShipDate = i.ShipDate;
                    d.AddDelivery = i.AddDelivery;
                    d.Tel = i.Tel;

                    OrderIdOld = i.OrderID;
                }
                else
                {
                    OrderIdOld = i.OrderID;

                    d.Product += Environment.NewLine + i.Product;
                    d.Quantity += Environment.NewLine + i.Quantity;
                    d.Total = Utility.ConvertToCurrency(Utility.ConvertToDec(d.Total) + Utility.ConvertToDec(i.Total));

                }
            }

            if ((string.Equals(dto.Incom, "Checked") && !string.Equals(d.Total, d.Payed))
                            || (string.Equals(dto.Com, "Checked") && string.Equals(d.Total, d.Payed))
                            || (string.Equals(dto.Com, "Unchecked") && string.Equals(dto.Incom, "Unchecked")))
            {
                d.No = count++.ToString();
                result.Add(d);
            }

            return result;
        }
    }
}
