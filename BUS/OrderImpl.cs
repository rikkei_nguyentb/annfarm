﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using DAO;
using DAO.Model;
using DTO;

namespace BUS
{
    public class OrderImpl : IOrder
    {
        private readonly IOrderRepository dao;
        private readonly IProductRepository daoPro;
        private readonly ICustomerRepository cus;
        private readonly IDiscountRepository discount;

        public OrderImpl(IOrderRepository _dao, IProductRepository _daoPro, ICustomerRepository _cus, IDiscountRepository _discount)
        {
            this.dao = _dao;
            daoPro = _daoPro;
            cus = _cus;
            discount = _discount;
        }

        public OrderDTO GetOrder(string tel, string shipmentDateF, string shipmentDateT)
        {
            OrderDTO result = dao.GetOrder(tel, shipmentDateF, shipmentDateT);

            if (result == null)
            {
                return null;
            }

            decimal total = 0;

            foreach (OrderDetailDTO item in result.LstOrderDetail)
            {
                item.SubTotal = Utility.ConvertToCurrency(Utility.ConvertToDec(item.Price)
                                                            * Utility.ConvertToDec(item.Quantity)
                                                            - Utility.ConvertToDec(item.Discount));
                total += Utility.ConvertToDec(item.SubTotal);
                //item.No = (result.LstOrderDetail.IndexOf(item) + 1).ToString();
            }
            total += Utility.ConvertToDec(result.ShippmentFee);
            result.Total = Utility.ConvertToCurrency(total);

            return result;
        }

        private string CreateProduct(ProductModel model)
        {
            string productID = daoPro.GetProductID(model.ProductName);
            if (string.IsNullOrEmpty(productID))
            {
                string id = daoPro.GetNextID();
                model.ProductID = id;
                daoPro.CreateProduct(model);

                return id;
            }
            else
            {
                model.ProductID = productID;
                model.DateUpdate = DateTime.Now.ToString("yyyyMMdd");
                daoPro.UpdateProduct(model);

                return productID;
            }
        }

        private string createCustomer(CustomerModel model)
        {
            string customerID = cus.GetCustomerID(model.Tel);

            if (string.IsNullOrEmpty(customerID))
            {
                string id = cus.GetNextID();
                model.CustomerID = id;
                cus.Create(model);

                return id;
            }
            else
            {
                model.CustomerID = customerID;
                model.DateUpdate = DateTime.Now.ToString("yyyyMMdd");
                cus.Update(model);

                return customerID;
            }
        }

        public int InsertOrder(OrderDTO orderDTO)
        {
            int res;
            string customerID;

            //check exist
            if (GetOrder(orderDTO.Tel, orderDTO.ShippmentDateFrom, orderDTO.ShippmentDateTo) != null)
            {
                return 0;
            }

            //insert new customer
            customerID = createCustomer(new CustomerModel
            {
                DateCreate = DateTime.Now.ToString("yyyyMMdd"),
                Tel = orderDTO.Tel,
                FullName = orderDTO.FullName,
                Address = orderDTO.ShippmentAdd
            });

            orderDTO.OrderID = DateTime.Now.ToString("yyyyMMddHHmmss");

            //insert order
            res = dao.InsertOrderModel(new OrderModel
            {
                CustomerID = customerID,
                Payed = orderDTO.Payed,
                ShippmentAdd = orderDTO.ShippmentAdd,
                ShippmentFee = orderDTO.ShippmentFee,
                ShippmentDateFrom = orderDTO.ShippmentDateFrom,
                ShippmentDateTo = orderDTO.ShippmentDateTo,
                Remark = orderDTO.Remark,
                OrderID = orderDTO.OrderID,
                DateCreate = DateTime.Now.ToString("yyyyMMdd"),
                DestFlag = "0"
            });

            if (res == 0)
            {
                return 0;
            }

            //insert orderdetail
            return InsertOrderDetail(orderDTO.LstOrderDetail, orderDTO.OrderID);
        }

        private int InsertOrderDetail(BindingList<OrderDetailDTO> od, string orderID)
        {
            foreach (OrderDetailDTO item in od)
            {
                if (string.IsNullOrEmpty(item.ProductName))
                {
                    continue;
                }

                string productID = CreateProduct(new ProductModel
                                                {
                                                    DateCreate = DateTime.Now.ToString("yyyyMMdd"),
                                                    Price = item.Price,
                                                    ProductName = item.ProductName,
                                                });

                UpdateDiscount(new DiscountModel
                {
                    DateCreate = DateTime.Now.ToString("yyyyMMdd"),
                    Discount = item.Discount,
                    ProductID = productID,
                    Quantity = item.Quantity
                });

                int res = dao.InsertOrderDetailModel(new OrderDetailModel
                {
                    Discount = item.Discount,
                    Price = item.Price,
                    ProductID = productID,
                    Quantity = item.Quantity,
                    OrderID = orderID,
                    DateCreate = DateTime.Now.ToString("yyyyMMdd")

                });

                if (res == 0)
                {
                    return 0;
                }
            }

            return 1;
        }

        private void UpdateDiscount(DiscountModel model)
        {
            if (discount.Exits(model))
            {
                //update
                discount.Update(model);
            }
            else
            {
                //insert
                discount.Insert(model);
            }
        }

        public int UpdateOrder(OrderDTO orderDto)
        {
            try
            {
                int res;
                //insert new customer
                createCustomer(new CustomerModel
                {
                    DateCreate = DateTime.Now.ToString("yyyyMMdd"),
                    Tel = orderDto.Tel,
                    FullName = orderDto.FullName,
                    Address = orderDto.ShippmentAdd
                });

                res = dao.UpdateOrder(new OrderModel
                {
                    DateCreate = DateTime.Now.ToString("yyyyMMdd"),
                    OrderID = orderDto.OrderID,
                    Payed = orderDto.Payed,
                    Remark = orderDto.Remark,
                    ShippmentAdd = orderDto.ShippmentAdd,
                    ShippmentDateFrom = orderDto.ShippmentDateFrom,
                    ShippmentDateTo = orderDto.ShippmentDateTo,
                    ShippmentFee = orderDto.ShippmentFee
                });

                if (res ==0)
                {
                    return 0;
                }

                dao.DeleteOrderDetail(orderDto.OrderID);

                return InsertOrderDetail(orderDto.LstOrderDetail, orderDto.OrderID);

            }
            catch (Exception e)
            {
                return 0;
            }


        }

        public IEnumerable<string> GetProductName()
        {
            IEnumerable<string> res = daoPro.GetProductName();

            if (res == null)
            {
                return new List<string>();
            }

            return res;
        }

        public OrderDTO GetOrder(string orderID)
        {
            OrderDTO result = dao.GetOrder(orderID);

            if (result == null)
            {
                return new OrderDTO { };
            }

            return result;
        }

        public int DeleteOrder(string orderID)
        {
            dao.DeleteOrderDetail(orderID);
            return dao.DeleteOrder(orderID);
        }
    }
}
