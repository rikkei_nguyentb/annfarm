﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO;
using System.Globalization;
using System.ComponentModel;

namespace BUS
{
    public class StatisticalImpl : IStatistical
    {
        private readonly IStatisticalRepository staDao;

        public StatisticalImpl(IStatisticalRepository _staDao)
        {
            staDao = _staDao;
        }

        public BindingList<GetStaInfoDto> GetStatisticalDic(StatisticalDto dto)
        {
            BindingList<GetStaInfoDto> result = staDao.GetStatisticalDic(dto);

            return result;
        }
    }
}
