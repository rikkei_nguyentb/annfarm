﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public interface ILogin
    {
        Boolean IsLogin(string user, string pass);
        void Register(string user, string pass);
        int Update(string user, string pass);
    }
}
