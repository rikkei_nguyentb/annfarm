﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO;

namespace BUS
{
    public class Login : ILogin
    {
        private ILoginRepository dao;

        public Login(ILoginRepository dao)
        {
            this.dao = dao;
        }

        public bool IsLogin(string user, string pass)
        {
            return dao.isLogin(user, pass);
        }

        public void Register(string user, string pass)
        {
            UserDTO userdto = new UserDTO();
            userdto.user = user;
            userdto.pass = pass;

            dao.Register(userdto);
        }

        public int Update(string user, string pass)
        {
            UserDTO userdto = new UserDTO();
            userdto.user = user;
            userdto.pass = pass;

            return dao.Update(userdto);
        }
    }

}
