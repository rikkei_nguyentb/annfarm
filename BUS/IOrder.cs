﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace BUS
{
    public interface IOrder
    {
        int InsertOrder(OrderDTO orderDTO);
        int UpdateOrder(OrderDTO orderDto);
        OrderDTO GetOrder(string tel, string shippmentDateF, string shippmentDateTo);
        IEnumerable<string> GetProductName();
        OrderDTO GetOrder(string orderID);
        int DeleteOrder(string orderID);
    }
}
