﻿using System;
using System.Collections.Generic;
using DAO;
using DAO.Model;
using DTO.FormView;

namespace BUS
{
    public class UtilityImpl : IUtility
    {
        private readonly ICustomerRepository cusDao;
        private readonly IProductRepository proDao;
        private readonly IDiscountRepository disDao;

        public UtilityImpl(ICustomerRepository _cusDao, IProductRepository _proDao, IDiscountRepository _disDao)
        {
            cusDao = _cusDao;
            proDao = _proDao;
            disDao = _disDao;
        }

        public CustomerModel GetCustomer(string tel)
        {
            CustomerModel res = cusDao.GetCustomer(tel);

            if (res == null)
            {
                return new CustomerModel
                {
                    Address = string.Empty,
                    DateCreate = string.Empty,
                    DateUpdate = string.Empty,
                    FaceBook = string.Empty,
                    FullName = string.Empty,
                    Gender = string.Empty,
                    Tel = string.Empty
                };
            }

            return res;
        }

        public IEnumerable<CustomerModel> GetCustomer()
        {
            return cusDao.GetCustomer();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productID"></param>
        /// <param name="quantity"></param>
        /// <returns>if null, return new obj</returns>
        public DiscountModel GetDiscount(string productID, string quantity)
        {
            DiscountModel res = disDao.GetDiscount(productID, quantity);

            if (res == null)
            {
                return new DiscountModel
                {
                    Discount = string.Empty,
                    ProductID = string.Empty,
                    Quantity = string.Empty,
                    DateCreate = string.Empty,
                    DateUpdate = string.Empty
                };
            }

            return res;
        }

        public IEnumerable<ProductModel> GetProduct()
        {
            return proDao.GetProduct();
        }

        public bool UpdateCustomer(MstCustomerView vm)
        {
            int res;

            res = cusDao.Update(new CustomerModel
            {
                Address = vm.Add,
                CustomerID = vm.CustomerID,
                DestFlag = "0",
                FaceBook = vm.Facebook,
                FullName = vm.FullName,
                Gender = vm.Gender,
                Tel = vm.Tel,
            });

            return res > 0;
        }
    }
}
