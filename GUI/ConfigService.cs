﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using BUS;
using DAO;

namespace GUI
{
    public class ConfigService
    {
        public static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() => 
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<ILogin, Login>();
            container.RegisterType<IOrder, OrderImpl>();
            container.RegisterType<IOrderRepository, OrderRepository>();
            container.RegisterType<IListAll, ListAllImpl>();
            container.RegisterType<IListAllRepository, ListAllRepository>();
            container.RegisterType<IStatistical, StatisticalImpl>();
            container.RegisterType<IStatisticalRepository, StatisticalRepository>();
            container.RegisterType<IDiscountRepository, DiscountRepository>();
            container.RegisterType<IUtility, UtilityImpl>();
            container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<ICustomerRepository, CustomerRepository>();
            container.RegisterType<IForm2BUS, Form2Impl>();
            container.RegisterType<ILoginRepository, LoginRepositoryText>();
        }
        
    }
}
