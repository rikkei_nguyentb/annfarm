﻿using System;
using System.Globalization;
using System.Reflection;
using System.Windows.Forms;

namespace GUI
{
    public class FormCustom<T> : Form
    {
        private T obj { get; set; }

        public T ViewModel
        {
            get
            {
                return GetViewModel();
            }
            set
            {
                obj = value;
                SetViewModel();
            }
        }

        private void SetViewModel()
        {
            Set(Controls);
        }

        private T GetViewModel()
        {
            return Get(Controls);
        }

        public void LockForm(bool isLock)
        {
            LockControls(isLock, Controls);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isLock"></param>
        /// <param name="controls"></param>
        private void LockControls(bool isLock, Control.ControlCollection controls)
        {
            foreach (Control item in controls)
            {
                if (string.Equals(item.GetType().Name, "TextBox"))
                {
                    ((TextBox)item).ReadOnly = isLock;
                }
                else if (string.Equals(item.GetType().Name, "GroupBox")
                    || string.Equals(item.GetType().Name, "Panel"))
                {
                    LockControls(isLock, item.Controls);
                }
                else if (string.Equals(item.GetType().Name, "DataGridView"))
                {
                    ((DataGridView)item).ReadOnly = isLock;
                }
                else if (string.Equals(item.GetType().Name, "DateTimePicker"))
                {
                    ((DateTimePicker)item).Enabled = !isLock;

                }
                else if (string.Equals(item.GetType().Name, "RadioButton"))
                {
                    ((RadioButton)item).Enabled = !isLock;

                }
                continue;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctrs"></param>
        /// <returns></returns>
        private T Get(Control.ControlCollection ctrs)
        {
            foreach (PropertyInfo p in typeof(T).GetProperties())
            {
                foreach (Control ctr in ctrs)
                {
                    if (string.Equals(ctr.GetType().Name, "TextBox")
                        || string.Equals(ctr.GetType().Name, "AutoCompleteTextBox")
                        || string.Equals(ctr.GetType().Name, "DateTimePicker"))
                    {
                        if (string.Equals("txt" + p.Name, ctr.Name))
                        {
                            p.SetValue(obj, ctr.Text);
                        }
                    }
                    else if (string.Equals(ctr.GetType().Name, "GroupBox") 
                        || string.Equals(ctr.GetType().Name, "Panel"))
                    {
                        obj = Get(ctr.Controls);
                    }
                    else if (string.Equals(ctr.GetType().Name, "DataGridView"))
                    {
                        if (string.Equals("g" + p.Name, ctr.Name))
                        {
                            p.SetValue(obj, ((DataGridView)ctr).DataSource);
                        }
                    }
                    else if (string.Equals(ctr.GetType().Name, "CheckBox"))
                    {
                        if (string.Equals("chk" + p.Name, ctr.Name))
                        {
                            p.SetValue(obj, ((CheckBox)ctr).CheckState.ToString());
                        }
                    }
                    else if (string.Equals(ctr.GetType().Name, "ComboBox"))
                    {
                        if (string.Equals("cb" + p.Name, ctr.Name))
                        {
                            p.SetValue(obj, ((ComboBox)ctr).SelectedValue.ToString());
                        }
                    }
                    else if (string.Equals(ctr.GetType().Name, "RadioButton"))
                    {
                        if (((RadioButton)ctr).Checked && ctr.Name.Contains("rd" + p.Name))
                        {
                            p.SetValue(obj, ((RadioButton)ctr).Text);
                        }
                    }
                }
            }

            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctr"></param>
        private void SetItem(Control ctr)
        {
            foreach (PropertyInfo p in typeof(T).GetProperties())
            {
                if (string.Equals(ctr.Name, "txt" + p.Name))
                {
                    ctr.Text = p.GetValue(obj).ToString();
                }
            }
        }

        private void SetDateTimePicker(Control ctr)
        {
            foreach (PropertyInfo p in typeof(T).GetProperties())
            {
                if (string.Equals(ctr.Name, "txt" + p.Name) && !string.IsNullOrEmpty(p.GetValue(obj).ToString()))
                {
                    ((DateTimePicker)ctr).Value = DateTime.ParseExact(p.GetValue(obj).ToString(), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                }
            }
        }

        private void SetDataGridView(Control ctr)
        {
            foreach (PropertyInfo p in typeof(T).GetProperties())
            {
                if (string.Equals(ctr.Name, "g" + p.Name))
                {
                    ((DataGridView)ctr).DataSource = p.GetValue(obj);
                }
            }

            ((DataGridView)ctr).Refresh();
        }

        private void SetCheckbox(Control ctr)
        {
            foreach (PropertyInfo p in typeof(T).GetProperties())
            {
                if (string.Equals(ctr.Name, "chk" + p.Name) && p.GetValue(obj) != null)
                {
                    ((CheckBox)ctr).CheckState = string.Equals(p.GetValue(obj).ToString(), "Checked") ? CheckState.Checked : CheckState.Unchecked;
                }
            }
        }

        private void SetRadioButton(Control ctr)
        {
            foreach (PropertyInfo p in typeof(T).GetProperties())
            {
                if (ctr.Name.Contains("rd" + p.Name) && string.Equals(ctr.Text, p.GetValue(obj)))
                {
                    ((RadioButton)ctr).Checked = true;
                }
            }
        }

        private void Set(Control.ControlCollection ctrs)
        {
            foreach (Control ctr in ctrs)
            {
                if (string.Equals(ctr.GetType().Name, "TextBox")
                    || string.Equals(ctr.GetType().Name, "AutoCompleteTextBox"))
                {
                    SetItem(ctr);
                }
                else if (string.Equals(ctr.GetType().Name, "GroupBox")
                        || string.Equals(ctr.GetType().Name, "Panel"))
                {
                    Set(ctr.Controls);
                }
                else if (string.Equals(ctr.GetType().Name, "DataGridView"))
                {
                    SetDataGridView(ctr);
                }
                else if (string.Equals(ctr.GetType().Name, "DateTimePicker"))
                {
                    SetDateTimePicker(ctr);
                }
                else if (string.Equals(ctr.GetType().Name, "CheckBox"))
                {
                    SetCheckbox(ctr);
                }
                else if (string.Equals(ctr.GetType().Name, "RadioButton"))
                {
                    SetRadioButton(ctr);
                }
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            //prress escape key to close form
            if (ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                Close();
                return true;
            }

            //F1 - F8
            if ((int)keyData >= (int)Keys.F1 && (int)keyData <= (int)Keys.F8)
            {
                foreach (Control ctr in Controls)
                {
                    if (string.Equals(ctr.GetType().Name, "CommandButton"))
                    {
                        Button btn = ((CommandButton)ctr).btns[(int)keyData - (int)Keys.F1];
                        btn.Focus();
                        btn.PerformClick();

                        return true;
                    }
                }
            }           

            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
