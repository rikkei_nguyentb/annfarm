﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using static GUI.CommandButton;
using Unity;

namespace GUI
{
    public partial class FrmListAll : FormCustom<ListAllDTO>
    {
        private readonly IListAll bus;
        private readonly IOrder busOd;
        private readonly IUtility uti;

        public FrmListAll(IListAll _bus, IOrder od, IUtility _uti)
        {
            bus = _bus;
            busOd = od;
            uti = _uti;

            InitializeComponent();
            commandButton1.ClickButton += new ButtonClickedEventHandler(ButtonClick);
            Init();
            GetDetail();
        }

        private void txtProduct_Leave(object sender, EventArgs e)
        {

        }

        private void ButtonClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            switch (btn.Tag)
            {
                case Utility.CONST.SEARCH_BTN:
                    GetDetail();
                    break;
                case Utility.CONST.CLEAR_BTN:
                    Init();
                    break;
                case Utility.CONST.NEW_BTN:
                    FrmMenu frmMenu = (FrmMenu)Parent.Parent;
                    FrmInquiry frmInquiry = Program.container.Value.Resolve<FrmInquiry>();

                    frmMenu.ActiveMdiChild.Close();

                    frmInquiry.MdiParent = frmMenu;
                    frmInquiry.Dock = DockStyle.Fill;
                    frmInquiry.Show();
                    break;

                case Utility.CONST.OUPUT_EXCEL_BTN:
                    string date = txtShipDateFrom.Text + Utility.CONST.SEPARATE_DATE + txtShipDateTo.Text;
                    Utility.ExcelOutput(gLstDetail, "List_Order", date);
                    break;
            }
        }

        private void GetDetail()
        {
            ListAllDTO vm = ViewModel;
            commandButton1.RemoveButton(Utility.CONST.OUPUT_EXCEL_BTN);

            if (string.IsNullOrEmpty(vm.ShipDateFrom))
            {
                vm.ShipDateFrom = DateTime.Now.ToString("yyyy/MM/dd");
            }

            if (string.IsNullOrEmpty(vm.ShipDateTo))
            {
                vm.ShipDateTo = DateTime.Now.ToString("yyyy/MM/dd");
            }

            vm.LstDetail = bus.GetListAll(vm);

            if (vm.LstDetail.Count > 0)
            {
                commandButton1.AddButton(Utility.CONST.OUPUT_EXCEL_BTN, Utility.CONST.F4);
            }
            else
            {
                commandButton1.RemoveButton(Utility.CONST.OUPUT_EXCEL_BTN);
            }

            ViewModel = vm;
            SetColorGridview();            
        }

        private void SetColorGridview()
        {
            foreach (DataGridViewRow row in gLstDetail.Rows)
            {
                if (string.Compare(row.Cells["ShipDate"].Value == null ? string.Empty : row.Cells["ShipDate"].Value.ToString().Substring(15, 10), DateTime.Now.ToString("yyyy/MM/dd")) == 0
                    && string.IsNullOrEmpty(row.Cells["Payed"].Value == null ? string.Empty : row.Cells["Payed"].Value.ToString()))
                {
                    row.DefaultCellStyle.BackColor = Color.IndianRed;
                }
            }            
        }

        private void Init()
        {
            commandButton1.SetButtonText(new string[8] {
                Utility.CONST.SEARCH_BTN,
                Utility.CONST.NEW_BTN,
                "",
                "",
                "",
                "",
                "",
                Utility.CONST.CLEAR_BTN,
            });


            ViewModel = new ListAllDTO
            {
                FullName = string.Empty,
                Product = string.Empty,
                ShipDateFrom = DateTime.Now.ToString("yyyy/MM/dd"),
                ShipDateTo = DateTime.Now.ToString("yyyy/MM/dd"),
                Tel = string.Empty,
                Incom = "Checked",
                LstDetail = new BindingList<GridDTO>()
            };

            //txtProduct
            AutoCompleteStringCollection sc = new AutoCompleteStringCollection();

            foreach (string item in busOd.GetProductName())
            {
                sc.Add(item);
            }

            txtProduct.AutoCompleteCustomSource = sc;

            //txtTel
            sc = new AutoCompleteStringCollection();

            foreach (var item in uti.GetCustomer())
            {
                sc.Add(item.Tel);
            }

            txtTel.AutoCompleteCustomSource = sc;

            //txtFullName
            sc = new AutoCompleteStringCollection();

            foreach (var item in uti.GetCustomer())
            {
                sc.Add(item.FullName);
            }

            txtFullName.AutoCompleteCustomSource = sc;
        }

        private void gLstDetail_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = gLstDetail.CurrentCell.RowIndex;

            if (gLstDetail.Rows[index].Cells["OrderID"].Value == null)
            {
                return;
            }

            string orderID = gLstDetail.Rows[index].Cells["OrderID"].Value.ToString();

            FrmInquiry frmInquiry = Program.container.Value.Resolve<FrmInquiry>();

            frmInquiry.GetOrder(orderID);
            frmInquiry.FormBorderStyle = FormBorderStyle.FixedSingle;            
            frmInquiry.StartPosition = FormStartPosition.CenterParent;
            frmInquiry.ShowDialog();
            GetDetail();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            SearchCustomer search = Program.container.Value.Resolve<SearchCustomer>();
            search.ShowDialog();
            txtTel.Text = search.customer.Tel;
        }

        private void txtShipDateFrom_Leave(object sender, EventArgs e)
        {
            if (string.Compare(txtShipDateFrom.Text, txtShipDateTo.Text) > 0)
            {
                txtShipDateTo.Text = txtShipDateFrom.Text;
            }
        }

        private void txtShipDateTo_Leave(object sender, EventArgs e)
        {
            if (string.Compare(txtShipDateFrom.Text, txtShipDateTo.Text) > 0)
            {
                txtShipDateFrom.Text = txtShipDateTo.Text;
            }
        }

        private void FrmListAll_Resize(object sender, EventArgs e)
        {
            if (Width >= 1140)
            {
                gLstDetail.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            else
            {
                gLstDetail.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            }

        }
    }
}
