﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Unity;

namespace GUI
{
    public static class Program
    {

        public static Lazy<IUnityContainer> container = ConfigService.container;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Lazy< IUnityContainer> container = ConfigService.container;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            Application.Run(container.Value.Resolve<FrmMenu>());

            container.Value.Dispose();
        }
    }
}
