﻿namespace GUI
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMenu));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cUSTOMERToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lISTEDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iNQUIRYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sTATISTICALToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pURCHASEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iNVENTORYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mASTERToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.productToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.tstUserLogin = new System.Windows.Forms.ToolStripTextBox();
            this.tstDateTime = new System.Windows.Forms.ToolStripTextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cUSTOMERToolStripMenuItem,
            this.pURCHASEToolStripMenuItem,
            this.iNVENTORYToolStripMenuItem,
            this.mASTERToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(784, 44);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cUSTOMERToolStripMenuItem
            // 
            this.cUSTOMERToolStripMenuItem.AutoSize = false;
            this.cUSTOMERToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lISTEDToolStripMenuItem,
            this.iNQUIRYToolStripMenuItem,
            this.sTATISTICALToolStripMenuItem});
            this.cUSTOMERToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.cUSTOMERToolStripMenuItem.Name = "cUSTOMERToolStripMenuItem";
            this.cUSTOMERToolStripMenuItem.Size = new System.Drawing.Size(122, 40);
            this.cUSTOMERToolStripMenuItem.Text = "SALES";
            // 
            // lISTEDToolStripMenuItem
            // 
            this.lISTEDToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lISTEDToolStripMenuItem.Name = "lISTEDToolStripMenuItem";
            this.lISTEDToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.lISTEDToolStripMenuItem.Text = "LIST ORDER";
            this.lISTEDToolStripMenuItem.Click += new System.EventHandler(this.lISTEDToolStripMenuItem_Click);
            // 
            // iNQUIRYToolStripMenuItem
            // 
            this.iNQUIRYToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.iNQUIRYToolStripMenuItem.Name = "iNQUIRYToolStripMenuItem";
            this.iNQUIRYToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.iNQUIRYToolStripMenuItem.Text = "ORDER";
            this.iNQUIRYToolStripMenuItem.Click += new System.EventHandler(this.iNQUIRYToolStripMenuItem_Click);
            // 
            // sTATISTICALToolStripMenuItem
            // 
            this.sTATISTICALToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sTATISTICALToolStripMenuItem.Name = "sTATISTICALToolStripMenuItem";
            this.sTATISTICALToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.sTATISTICALToolStripMenuItem.Text = "STATISTICAL";
            this.sTATISTICALToolStripMenuItem.Click += new System.EventHandler(this.sTATISTICALToolStripMenuItem_Click);
            // 
            // pURCHASEToolStripMenuItem
            // 
            this.pURCHASEToolStripMenuItem.AutoSize = false;
            this.pURCHASEToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pURCHASEToolStripMenuItem.Name = "pURCHASEToolStripMenuItem";
            this.pURCHASEToolStripMenuItem.Size = new System.Drawing.Size(122, 40);
            this.pURCHASEToolStripMenuItem.Text = "PURCHASE";
            // 
            // iNVENTORYToolStripMenuItem
            // 
            this.iNVENTORYToolStripMenuItem.AutoSize = false;
            this.iNVENTORYToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iNVENTORYToolStripMenuItem.Name = "iNVENTORYToolStripMenuItem";
            this.iNVENTORYToolStripMenuItem.Size = new System.Drawing.Size(122, 40);
            this.iNVENTORYToolStripMenuItem.Text = "INVENTORY";
            // 
            // mASTERToolStripMenuItem
            // 
            this.mASTERToolStripMenuItem.AutoSize = false;
            this.mASTERToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem1,
            this.productToolStripMenuItem});
            this.mASTERToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mASTERToolStripMenuItem.Name = "mASTERToolStripMenuItem";
            this.mASTERToolStripMenuItem.Size = new System.Drawing.Size(122, 40);
            this.mASTERToolStripMenuItem.Text = "MASTER";
            // 
            // customerToolStripMenuItem1
            // 
            this.customerToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customerToolStripMenuItem1.Name = "customerToolStripMenuItem1";
            this.customerToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.customerToolStripMenuItem1.Text = "CUSTOMER";
            this.customerToolStripMenuItem1.Click += new System.EventHandler(this.customerToolStripMenuItem1_Click);
            // 
            // productToolStripMenuItem
            // 
            this.productToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productToolStripMenuItem.Name = "productToolStripMenuItem";
            this.productToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.productToolStripMenuItem.Text = "PRODUCT";
            this.productToolStripMenuItem.Click += new System.EventHandler(this.productToolStripMenuItem_Click);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tstUserLogin,
            this.tstDateTime});
            this.menuStrip2.Location = new System.Drawing.Point(0, 676);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(784, 24);
            this.menuStrip2.TabIndex = 2;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // tstUserLogin
            // 
            this.tstUserLogin.AutoSize = false;
            this.tstUserLogin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tstUserLogin.Enabled = false;
            this.tstUserLogin.Name = "tstUserLogin";
            this.tstUserLogin.ReadOnly = true;
            this.tstUserLogin.Size = new System.Drawing.Size(200, 20);
            // 
            // tstDateTime
            // 
            this.tstDateTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tstDateTime.Enabled = false;
            this.tstDateTime.Name = "tstDateTime";
            this.tstDateTime.ReadOnly = true;
            this.tstDateTime.Size = new System.Drawing.Size(200, 20);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(784, 700);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MinimumSize = new System.Drawing.Size(800, 439);
            this.Name = "FrmMenu";
            this.Text = "AnnFarm - Organic Food";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cUSTOMERToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lISTEDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iNQUIRYToolStripMenuItem;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripTextBox tstUserLogin;
        private System.Windows.Forms.ToolStripTextBox tstDateTime;
        private System.Windows.Forms.ToolStripMenuItem sTATISTICALToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pURCHASEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iNVENTORYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mASTERToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem productToolStripMenuItem;
    }
}