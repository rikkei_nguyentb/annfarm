﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using Unity;
using System.Data.Odbc;

namespace GUI
{
    public partial class FrmLogin : Form
    {
        private ILogin login;

        public FrmLogin(ILogin _login)
        {
            InitializeComponent();

            this.login = _login;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (login.IsLogin(txtUser.Text, txtPass.Text))
            {
                this.Visible = false;
                FrmMenu frm = Program.container.Value.Resolve<FrmMenu>();
                frm._userName = txtUser.Text;
                frm.Show();
            }

        }

        private void btn_regist_Click(object sender, EventArgs e)
        {
            login.Register(txtUser.Text, txtPass.Text);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            login.Update(txtUser.Text, txtPass.Text);
        }

        //    private void btnRegist_Click(object sender, EventArgs e)
        //    {
        //        register.Register(txtUser.Text, txtPass.Text);
        //    }
    }
}
