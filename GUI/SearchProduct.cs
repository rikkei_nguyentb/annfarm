﻿using BUS;
using DAO.Model;
using GUI.FormView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static GUI.CommandButton;

namespace GUI
{
    public partial class SearchProduct : FormCustom<ProductView>
    {
        private readonly IUtility uti;
        public ProductModel product { get; set; }

        public SearchProduct(IUtility _uti)
        {
            uti = _uti;
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
        }

        #region event
        private void gDetail_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectRow();
        }

        private void SearchCustomer_Load(object sender, EventArgs e)
        {
            Init();
            commandButton1.ClickButton += new ButtonClickedEventHandler(ButtonClick);
            Search();
        }
        #endregion

        #region used function

        /// <summary>
        /// 
        /// </summary>
        private void Init()
        {
            commandButton1.SetButtonText(new string[8] 
            {
                Utility.CONST.SEARCH_BTN,
                "",
                "",
                "",
                "",
                "",
                "",
                Utility.CONST.CLEAR_BTN
            });

            product = new ProductModel
            {
                DateCreate = string.Empty,
                DestFlag = string.Empty,
                DateUpdate = string.Empty,
                Price = string.Empty,
                ProductID = string.Empty,
                ProductName = string.Empty,
                Source = string.Empty,
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            switch (btn.Tag)
            {
                case Utility.CONST.SEARCH_BTN:
                    Search();
                    break;
                case Utility.CONST.SELECT_BTN:
                    SelectRow();
                    break;
            }
        }

        private void SelectRow()
        {
            if (gDetail.Rows[gDetail.CurrentCell.RowIndex].Cells["ProductID"].Value == null)
            {
                return;
            }

            string productID = gDetail.Rows[gDetail.CurrentCell.RowIndex].Cells["ProductID"].Value.ToString();
            string productName = gDetail.Rows[gDetail.CurrentCell.RowIndex].Cells["ProductName"].Value.ToString();

            product = uti.GetProduct()
                .Where(p => Equals(p.ProductID, productID) && Utility.StringContains(p.ProductName, productName))
                .FirstOrDefault();

            Close();
        }

        private void Search()
        {
            int count = 0;
            ProductView vm = ViewModel;

            IEnumerable<ProductModel> m = uti.GetProduct().Where(t => Utility.StringContains(t.ProductName, vm.ProductName) || string.Equals(t.ProductID, vm.ProductID)).ToList();

            if (m == null)
            {
                vm.Detail = new BindingList<ProductDetail>();
                ViewModel = vm;

                commandButton1.RemoveButton(Utility.CONST.SELECT_BTN);
                return;
            }

            BindingList<ProductDetail> lst = new BindingList<ProductDetail>();

            foreach (ProductModel item in m)
            {
                lst.Add(new ProductDetail
                {
                    Price = item.Price,
                    ProductName = item.ProductName,
                    ProductID = item.ProductID,
                    No = (++count).ToString()
                });
            }
            vm.Detail = lst;
            ViewModel = vm;

            commandButton1.AddButton(Utility.CONST.SELECT_BTN, Utility.CONST.F2);

        }
        #endregion
    }
}
