﻿using BUS;
using DAO.Model;
using DTO.FormView;
using GUI.FormView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static GUI.CommandButton;

namespace GUI
{
    public partial class SearchCustomer : FormCustom<CustomerView>
    {
        private readonly IUtility uti;
        public CustomerModel customer { get; set; }

        public SearchCustomer(IUtility _uti)
        {
            uti = _uti;
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
        }

        #region event
        private void gDetail_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectRow();
        }

        private void SearchCustomer_Load(object sender, EventArgs e)
        {
            Init();
            commandButton1.ClickButton += new ButtonClickedEventHandler(ButtonClick);
            Search();
        }
        #endregion

        #region used function

        /// <summary>
        /// 
        /// </summary>
        private void Init()
        {
            commandButton1.SetButtonText(new string[8] 
            {
                Utility.CONST.SEARCH_BTN,
                "",
                "",
                "",
                "",
                "",
                "",
                Utility.CONST.CLEAR_BTN
            });

            customer = new CustomerModel
            {
                Address = string.Empty,
                CustomerID = string.Empty,
                DateCreate = string.Empty,
                DateUpdate = string.Empty,
                DestFlag = string.Empty,
                FaceBook = string.Empty,
                FullName = string.Empty,
                Gender = string.Empty,
                Tel = string.Empty,
            };

            ViewModel = new CustomerView
            {
                Detail = new BindingList<CustomerDetail>(),
                FullName = string.Empty,
                Tel = string.Empty,
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            switch (btn.Tag)
            {
                case Utility.CONST.SEARCH_BTN:
                    Search();
                    break;
                case Utility.CONST.SELECT_BTN:
                    SelectRow();
                    break;
            }
        }

        private void SelectRow()
        {
            object result;
            result = gDetail.Rows[gDetail.CurrentCell.RowIndex].Cells["Tel"].Value;

            if (result == null)
            {
                return;
            }

            customer = uti.GetCustomer(result.ToString());

            Close();
        }

        private void Search()
        {
            int count = 0;
            CustomerView vm = ViewModel;

            IEnumerable<CustomerModel> m = uti.GetCustomer().Where(t => Utility.StringContains(t.FullName, vm.FullName) || string.Equals(t.Tel, vm.Tel)).ToList();

            if (m == null)
            {
                vm.Detail = new BindingList<CustomerDetail>();
                ViewModel = vm;

                commandButton1.RemoveButton(Utility.CONST.SELECT_BTN);
                return;
            }

            BindingList<CustomerDetail> lst = new BindingList<CustomerDetail>();

            foreach (CustomerModel item in m)
            {
                lst.Add(new CustomerDetail
                {
                    Address = item.Address,
                    FullName = item.FullName,
                    Tel = item.Tel,
                    No = (++count).ToString()
                });
            }
            vm.Detail = lst;
            ViewModel = vm;

            commandButton1.AddButton(Utility.CONST.SELECT_BTN, Utility.CONST.F2);

        }
        #endregion
    }
}
