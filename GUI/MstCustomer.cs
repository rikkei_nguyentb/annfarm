﻿using BUS;
using DAO.Model;
using GUI.FormView;
using System;
using System.Windows.Forms;
using Unity;
using System.Linq;
using System.Globalization;
using DTO.FormView;
using static GUI.CommandButton;

namespace GUI
{
    public partial class MstCustomer : FormCustom<MstCustomerView>
    {
        private readonly IUtility uti;

        public MstCustomer(IUtility _uti)
        {
            uti = _uti;
            InitializeComponent();
            commandButton1.ClickButton += new ButtonClickedEventHandler(ButtonClick);
        }


        #region event

        private void txtCustomerID_Leave(object sender, EventArgs e)
        {
            Search();
        }

        private void MstCustomer_Load(object sender, EventArgs e)
        {
            Init();
        }

        private void label2_Click(object sender, EventArgs e)
        {            
            SearchCustomer search = Program.container.Value.Resolve<SearchCustomer>();
            search.ShowDialog();

            if (string.IsNullOrEmpty(search.customer.CustomerID))
            {
                return;
            }

            txtCustomerID.Text = search.customer.CustomerID;
            Search();
        }
        #endregion

        #region used function

        private void Init()
        {
            ViewModel = new MstCustomerView
            {
                Add = string.Empty,
                Birthday = string.Empty,
                CustomerID = string.Empty,
                DayJoin = string.Empty,
                Facebook = string.Empty,
                FullName = string.Empty,
                Gender = "Male",
                Tel = string.Empty,
            };

            commandButton1.SetButtonText(new string[] 
            {
                Utility.CONST.SEARCH_BTN,
                "",
                "",
                "",
                "",
                "",
                "",
                Utility.CONST.CLEAR_BTN,
            });

            LockForm(true);
            txtCustomerID.ReadOnly = false;
        }

        private void ButtonClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            switch (btn.Tag)
            {
                case Utility.CONST.SEARCH_BTN:
                    Search();
                    break;
                case Utility.CONST.CLEAR_BTN:
                    Init();
                    break;
                case Utility.CONST.UPDATE_BTN:
                    UpdateCustomer();
                    break;
            }
        }

        private void UpdateCustomer()
        {
            MstCustomerView vm = ViewModel;

            if (uti.UpdateCustomer(vm))
            {
                MessageBox.Show("Update Success !", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Init();
                return;
            }

            MessageBox.Show("Update Error !", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void Search()
        {
            MstCustomerView vm = ViewModel;

            CustomerModel cusModel = uti.GetCustomer().Where(c => string.Equals(c.CustomerID, vm.CustomerID)).SingleOrDefault();

            if (cusModel == null)
            {
                MessageBox.Show("CustomerID " + vm.CustomerID + " Not Found", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                commandButton1.RemoveButton(Utility.CONST.UPDATE_BTN);
                return;
            }

            ViewModel = new MstCustomerView
            {
                Add = cusModel.Address,
                Birthday = string.Empty,
                CustomerID = cusModel.CustomerID,
                DayJoin = DateTime.ParseExact(cusModel.DateCreate, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy/MM/dd"),
                Facebook = cusModel.FaceBook,
                FullName = cusModel.FullName,
                Gender = cusModel.Gender,
                Tel = cusModel.Tel,
            };

            commandButton1.AddButton(Utility.CONST.UPDATE_BTN, Utility.CONST.F6);
            LockForm(false);
            txtCustomerID.ReadOnly = true;
        }

        #endregion
    }
}
