﻿using BUS;
using DAO.Model;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using static GUI.CommandButton;
using Unity;

namespace GUI
{
    public partial class FrmInquiry : FormCustom<OrderDTO>
    {
        private readonly IOrder order;
        private readonly IUtility uti;
        private bool isEdit = false;

        public FrmInquiry(IOrder _order, IUtility _uti)
        {
            this.order = _order;
            uti = _uti;
            InitializeComponent();
            commandButton1.ClickButton += new ButtonClickedEventHandler(ButtonClick);
            Init();
        }

        #region event handled
        private void txtShippmentDateFrom_Leave(object sender, EventArgs e)
        {
            if (string.Compare(txtShippmentDateFrom.Text, txtShippmentDateTo.Text) > 0)
            {
                txtShippmentDateTo.Text = txtShippmentDateFrom.Text;
            }

            GetOrder();
        }

        private void txtShippmentDateTo_Leave(object sender, EventArgs e)
        {
            if (string.Compare(txtShippmentDateFrom.Text, txtShippmentDateTo.Text) > 0)
            {
                txtShippmentDateFrom.Text = txtShippmentDateTo.Text;
            }

            GetOrder();
        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gLstOrderDetail.CurrentCell.ColumnIndex == 0)
            {
                gLstOrderDetail.Rows[gLstOrderDetail.CurrentCell.RowIndex].Selected = true;
            }
        }

        private void dataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            for (int count = 1; count <= gLstOrderDetail.Rows.Count; count++)
            {
                gLstOrderDetail.Rows[count - 1].Cells[0].Value = count;
            }
        }

        private void dataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is TextBox auto_text)
            {
                auto_text.AutoCompleteMode = AutoCompleteMode.None;

                if (gLstOrderDetail.Columns[gLstOrderDetail.CurrentCell.ColumnIndex].HeaderText.Equals("Product"))
                {
                    auto_text.AutoCompleteMode = AutoCompleteMode.Suggest;
                    auto_text.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    AutoCompleteStringCollection sc = new AutoCompleteStringCollection();
                    addItems(sc);
                    auto_text.AutoCompleteCustomSource = sc;
                }
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {
            SearchCustomer search = Program.container.Value.Resolve<SearchCustomer>();
            search.ShowDialog();
            txtTel.Text = search.customer.Tel;
            GetOrder();
        }

        private void dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (gLstOrderDetail.CurrentCell == null)
            {
                return;
            }

            int irow = gLstOrderDetail.CurrentCell.RowIndex;

            OrderDTO vm = ViewModel;

            if (string.IsNullOrEmpty(vm.LstOrderDetail[irow].ProductName))
            {
                foreach (var prop in vm.LstOrderDetail[irow].GetType().GetProperties())
                {
                    prop.SetValue(vm.LstOrderDetail[irow], string.Empty);
                }
            }
            //suggest price
            if (string.Equals(gLstOrderDetail.Columns[gLstOrderDetail.CurrentCell.ColumnIndex].HeaderText, "Product"))
            {
                vm.LstOrderDetail[irow].Price = GetPrice(vm.LstOrderDetail[irow].ProductName);
            }

            //suggest discount
            if (string.Equals(gLstOrderDetail.Columns[gLstOrderDetail.CurrentCell.ColumnIndex].HeaderText, "Quantity"))
            {
                vm.LstOrderDetail[irow].Discount = GetDiscount(vm.LstOrderDetail[irow].ProductName, vm.LstOrderDetail[irow].Quantity);
            }

            if (isValidRow())
            {
                vm = SetSum(vm);
            }

            ViewModel = vm;
        }

        private string GetDiscount(string productName, string quantity)
        {
            string productID = uti.GetProduct()
                .Where(t => string.Equals(t.ProductName, productName))
                .Select(t => t.ProductID)
                .FirstOrDefault();

            return uti.GetDiscount(productID, quantity).Discount;
        }

        private string GetPrice(string productName)
        {
            return uti.GetProduct()
                .Where(t => string.Equals(t.ProductName, productName))
                .Select(t => t.Price)
                .FirstOrDefault();
        }

        private void ButtonClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            OrderDTO vm = ViewModel;

            isEdit = false;

            switch (btn.Tag)
            {
                case Utility.CONST.REGIST_BTN:
                    if (!CheckRegist(vm))
                    {
                        MessageBox.Show("Input Chua Dung", "Loi Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (order.InsertOrder(vm) != 1)
                    {
                        MessageBox.Show("Khong Dang Ky Duoc", "Loi Dang Ky", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Dang Ky Thanh Cong", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Init();
                    }

                    break;

                case Utility.CONST.UPDATE_BTN:
                    if (!CheckRegist(vm))
                    {
                        MessageBox.Show("Input Chua Dung", "Loi Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (order.UpdateOrder(vm) != 1)
                    {
                        MessageBox.Show("Khong Update Duoc", "Loi Update", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Update Thanh Cong", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GetOrder();
                    }

                    break;

                case Utility.CONST.DELETE_BTN:

                    if (order.DeleteOrder(vm.OrderID) != 1)
                    {
                        MessageBox.Show("Khong Delete Duoc", "Loi Delete", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Delete Thanh Cong", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Init();
                    }

                    break;

                case Utility.CONST.EDIT_BTN:
                    commandButton1.RemoveButton(Utility.CONST.EDIT_BTN);
                    commandButton1.AddButton(Utility.CONST.UPDATE_BTN, Utility.CONST.F6);
                    commandButton1.AddButton(Utility.CONST.UPDATE_BTN, Utility.CONST.F7);

                    isEdit = true;

                    LockForm(false);
                    txtTotal.ReadOnly = true;
                    gLstOrderDetail.Columns["No"].ReadOnly = true;
                    gLstOrderDetail.Columns["SubTotal"].ReadOnly = true;

                    break;

                case Utility.CONST.REMOVE_BTN:
                    if (gLstOrderDetail.RowCount == 1)
                    {
                        return;
                    }
                    int irow = gLstOrderDetail.CurrentCell.RowIndex;

                    vm.LstOrderDetail.Remove(vm.LstOrderDetail[irow]);
                    vm = SetSum(vm);

                    ViewModel = vm;

                    break;

                case Utility.CONST.CLEAR_BTN:
                    Init();
                    break;
            }
        }

        private void txtTel_Leave(object sender, EventArgs e)
        {
            GetOrder();
        }

        private void txtPayed_TextChanged(object sender, EventArgs e)
        {
            if (Utility.IsNumeric(txtPayed.Text))
            {
                txtPayed.Text = Utility.ConvertToCurrency(txtPayed.Text);
            }
            txtPayed.SelectionStart = txtPayed.Text.Length;
            txtPayed.SelectionLength = 0;
        }

        private void txtShippmentFee_Leave(object sender, EventArgs e)
        {
            ViewModel = SetSum(ViewModel);
        }

        private void txtShippmentFee_TextChanged(object sender, EventArgs e)
        {
            if (Utility.IsNumeric(txtShippmentFee.Text))
            {
                txtShippmentFee.Text = Utility.ConvertToCurrency(txtShippmentFee.Text);
            }
            txtShippmentFee.SelectionStart = txtShippmentFee.Text.Length;
            txtShippmentFee.SelectionLength = 0;
        }
        #endregion

        #region useful function

        private bool CheckRegist(OrderDTO order)
        {
            if (order.LstOrderDetail.Count == 0)
            {
                return false;
            }

            if (string.IsNullOrEmpty(order.ShippmentDateFrom))
            {
                return false;
            }

            return isValidRow();
        }

        /// <summary>
        /// 
        /// </summary>
        private void Init()
        {
            gLstOrderDetail.Columns["Product"].Selected = false;

            commandButton1.SetButtonText(new string[8] {
                "",
                "",
                Utility.CONST.REMOVE_BTN,
                "",
                "",
                "",
                "",
                Utility.CONST.CLEAR_BTN
            });

            ViewModel = new OrderDTO()
            {
                FullName = "",
                Payed = "",
                Remark = "",
                ShippmentAdd = "",
                ShippmentDateFrom = DateTime.Now.ToString("yyyy/MM/dd"),
                ShippmentDateTo = DateTime.Now.ToString("yyyy/MM/dd"),
                ShippmentFee = "",
                Tel = "",
                Total = "",
                OrderID = string.Empty,
                LstOrderDetail = new BindingList<OrderDetailDTO>()
            };

            AutoCompleteStringCollection sc = new AutoCompleteStringCollection();

            foreach (var item in uti.GetCustomer())
            {
                sc.Add(item.Tel);
            }

            txtTel.AutoCompleteCustomSource = sc;

            LockForm(true);
            txtTel.ReadOnly = false;
            txtShippmentDateFrom.Enabled = true;
            txtShippmentDateTo.Enabled = true;
        }

        public void GetOrder(string orderID)
        {
            ViewModel = order.GetOrder(orderID);

            //update
            commandButton1.AddButton(Utility.CONST.UPDATE_BTN, Utility.CONST.F6);
            commandButton1.AddButton(Utility.CONST.DELETE_BTN, Utility.CONST.F7);

            isEdit = true;

            LockForm(false);
            txtTotal.ReadOnly = true;
            gLstOrderDetail.Columns["No"].ReadOnly = true;
            gLstOrderDetail.Columns["SubTotal"].ReadOnly = true;
        }

        public void GetOrder()
        {
            if (isEdit || string.IsNullOrEmpty(txtTel.Text) || string.IsNullOrEmpty(txtShippmentDateFrom.Text))
            {
                return;
            }

            OrderDTO orderView = order.GetOrder(txtTel.Text, txtShippmentDateFrom.Text, txtShippmentDateTo.Text);

            commandButton1.RemoveButton(Utility.CONST.REGIST_BTN);
            commandButton1.RemoveButton(Utility.CONST.UPDATE_BTN);
            commandButton1.RemoveButton(Utility.CONST.DELETE_BTN);
            commandButton1.RemoveButton(Utility.CONST.EDIT_BTN);

            if (orderView == null)
            {
                //insert
                commandButton1.AddButton(Utility.CONST.REGIST_BTN, Utility.CONST.F5);

                OrderDTO t = ViewModel;
                CustomerModel us = uti.GetCustomer(t.Tel);
                t.FullName = us.FullName;
                t.ShippmentAdd = us.Address;
                t.Total = string.Empty;
                t.LstOrderDetail = new BindingList<OrderDetailDTO>();

                ViewModel = t;

                LockForm(false);
                txtTotal.ReadOnly = true;
                gLstOrderDetail.Columns["No"].ReadOnly = true;
                gLstOrderDetail.Columns["SubTotal"].ReadOnly = true;
            }
            else
            {
                //update
                commandButton1.AddButton(Utility.CONST.EDIT_BTN, Utility.CONST.F6);

                ViewModel = orderView;

                LockForm(true);
                txtTel.ReadOnly = false;
                txtShippmentDateFrom.Enabled = true;
                txtShippmentDateTo.Enabled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columns"></param>
        private void addItems(AutoCompleteStringCollection columns)
        {
            foreach (string product in order.GetProductName())
            {
                columns.Add(product);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private OrderDTO SetSum(OrderDTO vm)
        {
            decimal result = 0;

            foreach (OrderDetailDTO item in vm.LstOrderDetail)
            {
                item.Discount = Utility.ConvertToCurrency(item.Discount);
                item.Price = Utility.ConvertToCurrency(item.Price);
                item.SubTotal = Utility.ConvertToCurrency(Utility.ConvertToDec(item.Price) * Utility.ConvertToDec(item.Quantity) - Utility.ConvertToDec(item.Discount));

                result += Utility.ConvertToDec(item.SubTotal);
            }

            vm.Total = Utility.ConvertToCurrency(result + Utility.ConvertToDec(txtShippmentFee.Text));

            return vm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private bool isValidRow()
        {
            for (int row = 0; row < gLstOrderDetail.RowCount; row++)
            {
                if (!Utility.IsObjNullOrEmpty(gLstOrderDetail.Rows[row].Cells["Product"].Value)
                    && (Utility.IsObjNullOrEmpty(gLstOrderDetail.Rows[row].Cells["Price"].Value)
                        || Utility.IsObjNullOrEmpty(gLstOrderDetail.Rows[row].Cells["Quantity"].Value)
                    ))
                {
                    return false;
                }

                if (!Utility.IsNumeric(gLstOrderDetail.Rows[row].Cells["Price"].Value)
                    || !Utility.IsNumeric(gLstOrderDetail.Rows[row].Cells["Quantity"].Value)
                    || !Utility.IsNumeric(gLstOrderDetail.Rows[row].Cells["Discount"].Value))
                {
                    return false;
                }

            }

            return true;
        }

        #endregion
    }
}
