﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Unity;
using BUS;

namespace GUI
{
    public partial class FrmMenu : Form
    {
        private IForm2BUS form2;
        public string _userName { get; set; }

        public FrmMenu(IForm2BUS form2)
        {
            InitializeComponent();
            CenterToScreen();
            this.form2 = form2;
            tstUserLogin.Text = "User : " + _userName;
            lISTEDToolStripMenuItem.PerformClick();

            timer1.Tick += new EventHandler(timer1_Tick);
        }
        
        private void lISTEDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowForm(Program.container.Value.Resolve<FrmListAll>());        
        }

        private void iNQUIRYToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowForm(Program.container.Value.Resolve<FrmInquiry>());
        }

        private void sTATISTICALToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowForm(Program.container.Value.Resolve<FrmStatistical>());
        }

        private void ShowForm(Form frm)
        {
            CloseActiveForm();

            frm.MdiParent = this;
            frm.Dock = DockStyle.Fill;
            frm.Show();
        }

        private void CloseActiveForm()
        {
            if (ActiveMdiChild != null)
            {
                ActiveMdiChild.Close();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tstDateTime.Text = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        }

        private void customerToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ShowForm(Program.container.Value.Resolve<MstCustomer>());
        }

        private void productToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowForm(Program.container.Value.Resolve<MstProduct>());
        }
    }
}
