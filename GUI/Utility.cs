﻿using System;
using System.Drawing;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;

namespace GUI
{
    public static class Utility
    {   
        public struct CONST
        {
            public const string SEPARATE_DATE = "  ~  ";
            public const string SEARCH_BTN = "Search";
            public const string UPDATE_BTN = "Update";
            public const string REGIST_BTN = "Create";
            public const string DELETE_BTN = "Delete";
            public const string REMOVE_BTN = "Remove";
            public const string CLEAR_BTN = "Clear";
            public const string OUPUT_EXCEL_BTN = "Output Excel";
            public const string NEW_BTN = "New";
            public const string SELECT_BTN = "Select";
            public const string EDIT_BTN = "Edit";

            public const int F1 = 0;
            public const int F2 = 1;
            public const int F3 = 2;
            public const int F4 = 3;
            public const int F5 = 4;
            public const int F6 = 5;
            public const int F7 = 6;
            public const int F8 = 7;

        }  
        
        public static bool StringContains(string s1, string s2)
        {
            if (s2 == null)
            {
                return true;
            }
            return ConvertToUnSign(s1.ToLower()).Contains(ConvertToUnSign(s2.ToLower()));
        }

        private static string ConvertToUnSign(string input)
        {
            input = input.Trim();
            for (int i = 0x20; i < 0x30; i++)
            {
                input = input.Replace(((char)i).ToString(), " ");
            }
            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string str = input.Normalize(NormalizationForm.FormD);
            string str2 = regex.Replace(str, string.Empty).Replace('đ', 'd').Replace('Đ', 'D');
            while (str2.IndexOf("?") >= 0)
            {
                str2 = str2.Remove(str2.IndexOf("?"), 1);
            }
            return str2;
        }

        internal static bool StrEquals(object o1, object o2)
        {
            if (o1 == o2)
            {
                return true;
            }

            if (o1 != null || o2 != null)
            {
                return false;
            }

            return string.Equals(o1.ToString(), o2.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertToCurrency(object value)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString()) || string.Equals(value.ToString(), "0"))
            {
                return string.Empty;
            }

            return string.Format("{0:0,0}", ConvertToDec(value));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNumeric(object value)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString()))
            {
                return true;
            }

            return decimal.TryParse(value.ToString(), System.Globalization.NumberStyles.Currency, CultureInfo.InvariantCulture, out decimal result);
        }

        public static bool IsObjNullOrEmpty(object obj)
        {
            if (obj == null)
            {
                return true;
            }

            if (string.IsNullOrEmpty(obj.ToString()))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal ConvertToDec(object value)
        {
            decimal result;

            if (value == null || string.IsNullOrEmpty(value.ToString()))
            {
                return 0;
            }

            decimal.TryParse(value.ToString(), NumberStyles.Currency, CultureInfo.InvariantCulture, out result);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controls"></param>
        public static void ClearScreen(Control.ControlCollection controls)
        {
            foreach (Control item in controls)
            {
                if (!string.Equals(item.GetType().Name, "TextBox"))
                {
                    if (string.Equals(item.GetType().Name, "GroupBox"))
                    {
                        ClearScreen(item.Controls);
                    }
                    else if (string.Equals(item.GetType().Name, "DataGridView"))
                    {
                        ((DataGridView)item).DataSource = null;
                    }

                    continue;
                }

                ((System.Windows.Forms.TextBox)item).Text = "";
            }

        }

        /// <summary>
        /// Create excel file, choose path and save
        /// </summary>
        /// <param name="gv"></param>
        /// <param name="title"></param>
        /// <param name="date"></param>
        public static void ExcelOutput(DataGridView gv, string title, string date)
        {
            int iRow = 0;
            int iCol = 0;
            int colIgrone = 0;
            SaveFileDialog fd = new SaveFileDialog();
            Microsoft.Office.Interop.Excel.Application xlApp;
            Workbook xlWorkBook;
            Worksheet xlWorksheet;
            DataGridViewCell cell;

            //open confirm dialog
            if (MessageBox.Show("Do You Want To Output Excel File?", "Output Excel", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                return;
            }

            //choose path
            fd.FileName = title + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xlsx";
            fd.Filter = "Microsoft Excel (*.xlsx)|*.xlsx";
            fd.AddExtension = true;

            if (fd.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            
            //Create excel File
            xlApp = new Microsoft.Office.Interop.Excel.Application();

            if (xlApp == null)
            {
                MessageBox.Show("Excel is not properly installed!!", "Install Excel", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            xlWorkBook = xlApp.Workbooks.Add();
            xlWorksheet = (Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorksheet.Name = title;
            xlWorksheet.get_Range("A:A", "Z:Z").Interior.Color = ColorTranslator.ToOle(Color.White);

            for (iRow = 0; iRow < gv.RowCount; iRow++)
            {
                for (iCol = 0; iCol < gv.ColumnCount; iCol++)
                {
                    cell = gv.Rows[iRow].Cells[iCol];

                    if (!cell.Visible)
                    {
                        colIgrone++;
                        continue;
                    }

                    //set header
                    if (iRow == 0)
                    {
                        xlWorksheet.Cells[3, iCol + 1] = gv.Columns[iCol].HeaderText;
                        xlWorksheet.Range[xlWorksheet.Cells[3, iCol + 1], xlWorksheet.Cells[3, iCol + 1]].Font.Color = ColorTranslator.ToOle(Color.White);
                        xlWorksheet.Range[xlWorksheet.Cells[3, iCol + 1], xlWorksheet.Cells[3, iCol + 1]].Font.Bold = true;
                        xlWorksheet.Range[xlWorksheet.Cells[3, iCol + 1], xlWorksheet.Cells[3, iCol + 1]].Interior.Color = ColorTranslator.ToOle(Color.Green);
                    }

                    //set detail
                    xlWorksheet.Cells[4 + iRow, iCol + 1] = cell.Value;

                    if (cell.Value == null || !cell.Value.ToString().Contains(Environment.NewLine))
                    {
                        continue;
                    }

                    string tmp = cell.Value.ToString().Replace(Environment.NewLine, "");
                    if (IsNumeric(tmp))
                    {
                        xlWorksheet.Cells[4 + iRow, iCol + 1].HorizontalAlignment = XlHAlign.xlHAlignRight;
                    }
                }
            }
            colIgrone = colIgrone / iRow;

            //set footer

            //set option
            Range range = xlWorksheet.Range[xlWorksheet.Cells[3, 1], xlWorksheet.Cells[2 + iRow, iCol - colIgrone]];
            range.EntireColumn.AutoFit();
            range.Borders.LineStyle = XlLineStyle.xlContinuous;
            range.Borders.Weight = 2;

            //set title
            xlWorksheet.Cells[1, 1] = title + " In Range " + date;
            xlWorksheet.Range[xlWorksheet.Cells[1, 1], xlWorksheet.Cells[1, iCol - colIgrone]].Merge(false);
            xlWorksheet.Range[xlWorksheet.Cells[1, 1], xlWorksheet.Cells[1, iCol - colIgrone]].Font.Size = 18;
            xlWorksheet.Range[xlWorksheet.Cells[1, 1], xlWorksheet.Cells[1, iCol - colIgrone]].HorizontalAlignment = XlHAlign.xlHAlignCenter;

            //save file
            try
            {
                xlWorkBook.SaveAs(fd.FileNames);
                xlWorkBook.Close();
                xlApp.Quit();

                MessageBox.Show("Excel file created", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show(fd.FileNames + " Is exist", "Unsuccess", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Marshal.ReleaseComObject(xlWorksheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);
        }
    }
}
