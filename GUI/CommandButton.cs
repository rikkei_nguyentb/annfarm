﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GUI
{
    public partial class CommandButton : UserControl
    {
        const int MAX_BUTTON = 8;

        public delegate void ButtonClickedEventHandler(object sender, EventArgs e);
        public event ButtonClickedEventHandler ClickButton;
        public Button[] btns = new Button[MAX_BUTTON];

        public CommandButton()
        {
            for (int index = 0; index < MAX_BUTTON; index++)
            {
                btns[index] = new Button();

                btns[index].Location = new Point(92 * index + 5, 15);
                btns[index].Name = "button_" + index;
                btns[index].Size = new Size(90, 36);
                btns[index].TabIndex = index;
                btns[index].Text = "";
                btns[index].UseVisualStyleBackColor = true;

                Controls.Add(btns[index]);

                btns[index].Click += new EventHandler(OnButtonClick);

            }

            InitializeComponent();
            SetButtonText(new string[8] { "", "", "", "", "", "", "", "" });
        }

        public void OnButtonClick(object sender, EventArgs e)
        {
            // Delegate the event to the caller
            ClickButton(sender, e);
        }

        public void SetVisible(string btnName, bool isVisible)
        {
            foreach (Button btn in btns)
            {
                if (string.Equals(btnName, btn.Name))
                {
                    btn.Visible = isVisible;
                }
            }
        }

        public void AddButton(string btnName, int index)
        {
            btns[index].Text = btnName + Environment.NewLine + "( F" + (index + 1) + " )";
            btns[index].Tag = btnName;
            btns[index].Visible = true;
        }

        public void RemoveButton(string btnName)
        {
            foreach (Button btn in btns)
            {
                if (Utility.StrEquals(btn.Tag, btnName))
                {
                    btn.Text = string.Empty;
                    btn.Tag = string.Empty;
                    btn.Visible = false;
                }
            }
        }

        public void ChangeButtonName(string oldBtnName, string newBtnName)
        {
            foreach (Button btn in btns)
            {
                if (string.Equals(btn.Text, oldBtnName))
                {
                    btn.Text = newBtnName;
                    btn.Tag = newBtnName;
                }
            }
        }

        public void SetButtonEnabled(string btnName, bool isEnabled)
        {
            foreach (Button btn in btns)
            {
                if (string.Equals(btn.Text, btnName))
                {
                    btn.Enabled = isEnabled;
                }
            }
        }

        public void SetButtonText(string[] buttonText)
        {
            for (int i = 0; i < buttonText.Length; i++)
            {
                if (string.IsNullOrEmpty(buttonText[i]))
                {
                    btns[i].Visible = false;
                    continue;
                }

                btns[i].Text = buttonText[i] + Environment.NewLine + "( F" + (i + 1) + " )";
                btns[i].Tag = buttonText[i];
                btns[i].Visible = true;
            }
        }
    }
}
