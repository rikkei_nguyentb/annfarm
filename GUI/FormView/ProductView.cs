﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.FormView
{
    public class ProductView
    {
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public BindingList<ProductDetail> Detail { get; set; }
    }

    public class ProductDetail
    {
        public string No { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string Price { get; set; }
    }
}
