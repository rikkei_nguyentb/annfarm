﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.FormView
{
    public class CustomerView
    {
        public string Tel { get; set; }
        public string FullName { get; set; }
        public BindingList<CustomerDetail> Detail { get; set; }
    }

    public class CustomerDetail
    {
        public string No { get; set; }
        public string Tel { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
    }
}
