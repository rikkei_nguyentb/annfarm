﻿using BUS;
using DTO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static GUI.CommandButton;
using System.Windows.Forms.DataVisualization.Charting;
using Unity;

namespace GUI
{
    public partial class FrmStatistical : FormCustom<StatisticalDto>
    {
        private readonly IUtility uti;
        private readonly IStatistical sta;
        private readonly IListAll lstAll;

        Point? prevPosition = null;

        private int iShow = 5;

        public FrmStatistical(IUtility _uti, IStatistical _sta, IListAll _lstAll)
        {
            uti = _uti;
            sta = _sta;
            lstAll = _lstAll;
            InitializeComponent();
            Init();
            commandButton1.ClickButton += new ButtonClickedEventHandler(ButtonClick);
        }

        #region Event

        private void label2_Click(object sender, EventArgs e)
        {
            SearchProduct search = Program.container.Value.Resolve<SearchProduct>();
            search.ShowDialog();
            txtProduct.Text = search.product.ProductName;
        }

        private void ButtonClick(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            switch (btn.Tag)
            {
                case Utility.CONST.SEARCH_BTN:
                    Search();
                    break;

                case Utility.CONST.CLEAR_BTN:
                    Init();
                    break;

                case Utility.CONST.OUPUT_EXCEL_BTN:
                    string date = txtDateFrom.Text + Utility.CONST.SEPARATE_DATE + txtDateTo.Text;
                    Utility.ExcelOutput(gDetail, Text, date);
                    break;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            SearchCustomer search = Program.container.Value.Resolve<SearchCustomer>();
            search.ShowDialog();
            txtCustomerName.Text = search.customer.FullName;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.Equals(tabControl1.SelectedTab.Name, "Table") && gDetail.RowCount > 1)
            {
                commandButton1.AddButton(Utility.CONST.OUPUT_EXCEL_BTN, Utility.CONST.F4);
            }
            else
            {
                commandButton1.RemoveButton(Utility.CONST.OUPUT_EXCEL_BTN);
            }
        }

        private void FrmStatistical_Resize(object sender, EventArgs e)
        {
            int num = (Width - 370 - Width / 30)/100;

            if (num != iShow)
            {
                iShow = num;
                if (!string.IsNullOrEmpty(ViewModel.DateFrom))
                {
                    Search();
                }
            }
        }
        #endregion

        #region Private Function

        /// <summary>
        /// 
        /// </summary>
        private void Search()
        {
            StatisticalDto dto = ViewModel;
            ClearDetail();

            if (checkGrowthChart())
            {
                SetGrowthChart(dto);
            }

            SetTable(dto);
            SetTopCustomerChart(dto);
            SetTopProductChart(dto);

            if (string.Equals(tabControl1.SelectedTab.Name, "Table") && gDetail.RowCount > 1)
            {
                commandButton1.AddButton(Utility.CONST.OUPUT_EXCEL_BTN, Utility.CONST.F4);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        private void SetTopProductChart(StatisticalDto dto)
        {
            dto.CustomerName = string.Empty;
            dto.Product = string.Empty;
            BindingList<GetStaInfoDto> tmpDto = sta.GetStatisticalDic(dto);

            ResetChartTitle(chProduct);
            Dictionary<string, Dictionary<string, string>> dic = GetTopProductDic(tmpDto, dto);

            foreach (var series in dic)
            {
                chProduct.Series.Add(series.Key);
                foreach (var item in series.Value)
                {
                    chProduct.Series[series.Key].Points.AddXY(item.Key, item.Value);
                }

            }

            chProduct.Titles[0].Text = "Top "+ iShow + " Product Trong Khoang " + dto.DateFrom + "  ~  " + dto.DateTo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tmpDto"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        private Dictionary<string, Dictionary<string, string>> GetTopProductDic(BindingList<GetStaInfoDto> tmpDto, StatisticalDto dto)
        {
            Dictionary<string, Dictionary<string, string>> result = new Dictionary<string, Dictionary<string, string>>();
            Dictionary<string, string> subRes = new Dictionary<string, string>();

            string oldKey = string.Empty;
            string key = string.Empty;
            string value = string.Empty;

            foreach (GetStaInfoDto item in tmpDto.OrderBy(t => t.Product))
            {
                if (string.IsNullOrEmpty(item.Product))
                {
                    continue;
                }

                key = item.Product;
                value = item.Quantity;

                if (string.IsNullOrEmpty(oldKey) || !string.Equals(oldKey, key))
                {
                    subRes.Add(key, "0");
                }

                subRes[key] = Utility.ConvertToCurrency(Utility.ConvertToDec(subRes[key]) + Utility.ConvertToDec(value));
                oldKey = key;
            }

            //sort descending
            subRes = subRes.OrderByDescending(p => Utility.ConvertToDec(p.Value)).ToDictionary(p => p.Key, p => p.Value);
            //select top 10
            subRes = subRes.Take(iShow).ToDictionary(p => p.Key, p => p.Value);

            result.Add("KG", subRes);

            return result;
        }

        private Dictionary<string, string> getTopChart(Dictionary<string, string> chartDic, int numGet)
        {
            //sort descending
            chartDic = chartDic.OrderByDescending(p => Utility.ConvertToDec(p.Value)).ToDictionary(p => p.Key, p => p.Value);
            //select top 10
            chartDic = chartDic.Take(numGet).ToDictionary(p => p.Key, p => p.Value);

            return chartDic;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        private void SetTable(StatisticalDto dto)
        {
            BindingList<GetStaInfoDto> lst = sta.GetStatisticalDic(dto);

            gDetail.DataSource = lst;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        private void SetTopCustomerChart(StatisticalDto dto)
        {
            BindingList<GridDTO> tmpDto = lstAll.GetListAll(new ListAllDTO
            {
                Com = "Unchecked",
                FullName = string.Empty,
                Incom = "Unchecked",
                LstDetail = new BindingList<GridDTO>(),
                Product = string.Empty,
                ShipDateFrom = dto.DateFrom,
                ShipDateTo = dto.DateTo,
                Tel = string.Empty
            });

            if (string.IsNullOrEmpty(tmpDto[0].OrderID))
            {
                return;
            }
            ResetChartTitle(chCustomer);
            Dictionary<string, Dictionary<string, string>> dic = GetTopCustomerDic(tmpDto, dto);

            foreach (var series in dic)
            {
                chCustomer.Series.Add(series.Key);
                foreach (var item in series.Value)
                {
                    chCustomer.Series[series.Key].Points.AddXY(item.Key, item.Value);
                }

            }

            chCustomer.Titles[0].Text = "Top "+ iShow + " Customer Trong Khoang " + dto.DateFrom + Utility.CONST.SEPARATE_DATE + dto.DateTo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lst"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        private Dictionary<string, Dictionary<string, string>> GetTopCustomerDic(BindingList<GridDTO> lst, StatisticalDto dto)
        {
            Dictionary<string, Dictionary<string, string>> result = new Dictionary<string, Dictionary<string, string>>();
            Dictionary<string, string> subRes = new Dictionary<string, string>();

            string oldKey = string.Empty;
            string key = string.Empty;
            string value = string.Empty;

            foreach (GridDTO item in lst.OrderBy(t => t.FullName))
            {
                if (string.IsNullOrEmpty(item.OrderID))
                {
                    continue;
                }

                key = string.IsNullOrEmpty(item.FullName) ? item.Tel : item.FullName ;
                value = Utility.ConvertToCurrency(Utility.ConvertToDec(item.Total) / 1000);

                if (string.IsNullOrEmpty(oldKey) || !string.Equals(oldKey, key))
                {
                    subRes.Add(key, "0");
                }

                subRes[key] = Utility.ConvertToCurrency(Utility.ConvertToDec(subRes[key]) + Utility.ConvertToDec(value));
                oldKey = key;
            }

            //sort descending
            subRes = subRes.OrderByDescending(p => Utility.ConvertToDec(p.Value)).ToDictionary(p => p.Key, p => p.Value);
            //select top 10
            subRes = subRes.Take(iShow).ToDictionary(p => p.Key, p => p.Value);

            result.Add("1K-VND", subRes);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controls"></param>
        private void ClearChart(Control.ControlCollection controls)
        {
            foreach (Control ctr in controls)
            {
                if (string.Equals(ctr.GetType().Name, "Chart"))
                {
                    ((Chart)ctr).Series.Clear();
                    ((Chart)ctr).ResetAutoValues();
                    ((Chart)ctr).Titles[0].Text = "No Information";
                    ((Chart)ctr).Titles[0].DockingOffset = -50;
                    ((Chart)ctr).Titles[0].Font = new Font("Arial", 16, FontStyle.Bold);
                }

                if (string.Equals(ctr.GetType().Name, "TabControl")
                    || string.Equals(ctr.GetType().Name, "GroupBox")
                    || string.Equals(ctr.GetType().Name, "TabPage"))
                {
                    ClearChart(ctr.Controls);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        private void ResetChartTitle(Chart c)
        {
            c.Titles[0].DockingOffset = 0;
            c.Titles[0].Font = new Font("Arial", 8, FontStyle.Regular);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        private void SetGrowthChart(StatisticalDto dto)
        {
            BindingList<GridDTO> tmpDto = lstAll.GetListAll(new ListAllDTO
            {
                Com = "Unchecked",
                FullName = dto.CustomerName,
                Incom = "Unchecked",
                LstDetail = new BindingList<GridDTO>(),
                Product = dto.Product,
                ShipDateFrom = dto.DateFrom,
                ShipDateTo = dto.DateTo,
                Tel = string.Empty
            });

            if (string.IsNullOrEmpty(tmpDto[0].OrderID))
            {
                return;
            }

            ResetChartTitle(chart1);

            Dictionary<string, Dictionary<string, string>> dic = GetStatisticalDic(tmpDto, dto);

            foreach (var series in dic)
            {
                chart1.Series.Add(series.Key);
                foreach (var item in series.Value)
                {
                    chart1.Series[series.Key].Points.AddXY(item.Key, item.Value);
                }

            }

            chart1.Titles[0].Text = "Thong Ke Cua " + getKindOfStatis(dto)
                + " Trong Khoang " + dto.DateFrom + "  ~  " + dto.DateTo
                + " Theo " + dto.Type;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private string getKindOfStatis(StatisticalDto dto)
        {
            if (string.IsNullOrEmpty(dto.CustomerName))
            {
                return dto.Product;
            }
            else if (string.IsNullOrEmpty(dto.Product))
            {
                return dto.CustomerName;
            }
            else
            {
                return dto.CustomerName + "  Mua  " + dto.Product;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lst"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        private Dictionary<string, Dictionary<string, string>> GetStatisticalDic(BindingList<GridDTO> lst, StatisticalDto dto)
        {
            Dictionary<string, Dictionary<string, string>> result = new Dictionary<string, Dictionary<string, string>>();
            Dictionary<string, string> subRes = new Dictionary<string, string>();

            string oldKey = string.Empty;
            string key = string.Empty;
            string standardKey = string.Empty;
            string value = string.Empty;

            string typeStt = getTypeStatis(dto);

            foreach (GridDTO item in lst.OrderBy(t => t.ShipDate))
            {
                if (string.IsNullOrEmpty(item.OrderID))
                {
                    continue;
                }

                switch (dto.Type)
                {
                    case "Quater":
                        key = (int.Parse(item.ShipDate.Substring(5, 2)) / 3 + 1).ToString() + "th";
                        standardKey = (int.Parse(dto.DateFrom.Substring(5, 2)) / 3 + 1).ToString() + "th";
                        break;

                    case "Month":
                        key = getMonth(item.ShipDate.Substring(5, 2));
                        standardKey = getMonth(dto.DateFrom.Substring(5, 2));
                        break;

                    case "Week":
                        key = DateTime.ParseExact(item.ShipDate.Substring(0, 10), "yyyy/MM/dd", CultureInfo.InvariantCulture).DayOfYear / 7 + "th";
                        standardKey = DateTime.ParseExact(dto.DateFrom.Substring(0, 10), "yyyy/MM/dd", CultureInfo.InvariantCulture).DayOfYear / 7 + "th";
                        break;

                    default:
                        key = "ALL";
                        break;
                }

                switch (typeStt)
                {
                    case "Product":
                        value = item.Quantity;
                        break;

                    case "Customer":
                        value = Utility.ConvertToCurrency(Utility.ConvertToDec(item.Total) / 1000);
                        break;

                    default:
                        value = item.Quantity;
                        break;
                }

                if (string.IsNullOrEmpty(oldKey) || !string.Equals(oldKey, key))
                {
                    while (!string.Equals(key, getContinueKey(oldKey, dto)))
                    {
                        subRes.Add(getContinueKey(oldKey, dto), "0");
                        oldKey = getContinueKey(oldKey, dto);
                    }

                    subRes.Add(key, "0");
                }

                subRes[key] = Utility.ConvertToCurrency(Utility.ConvertToDec(subRes[key]) + Utility.ConvertToDec(value));
                oldKey = key;
            }

            while (!(string.Equals(getEndKey(dto), key) || string.Equals(getEndKey(dto), getContinueKey(key, dto)) || subRes.ContainsKey(getContinueKey(key, dto))))
            {
                subRes.Add(getContinueKey(key, dto), "0");
                key = getContinueKey(key, dto);
            }

            result.Add(GetSeries(typeStt), subRes);

            return result;
        }

        private string getEndKey(StatisticalDto dto)
        {
            switch (dto.Type)
            {
                case "Quater":
                    return (int.Parse(dto.DateTo.Substring(5, 2)) / 3 + 1).ToString() + "th";

                case "Month":
                    return getMonth(dto.DateTo.Substring(5, 2));
                case "Week":
                     return DateTime.ParseExact(dto.DateTo.Substring(0, 10), "yyyy/MM/dd", CultureInfo.InvariantCulture).DayOfYear / 7 + "th";
                case "ALL":
                    return "ALL";

                default:
                    return string.Empty;
            }
        }

        private string getContinueKey(string key, StatisticalDto dto)
        {
            switch (dto.Type)
            {
                case "Quater":
                    if (string.IsNullOrEmpty(key))
                    {
                        return (int.Parse(dto.DateFrom.Substring(5, 2)) / 3 + 1).ToString() + "th";
                    }
                    return (Utility.ConvertToDec(key.Replace("th", "")) % 4 + 1).ToString() + "th";

                case "Month":
                    string[] monthNames = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Jan" };

                    if (string.IsNullOrEmpty(key))
                    {
                        return getMonth(dto.DateFrom.Substring(5, 2));
                    }

                    return monthNames[Array.IndexOf(monthNames, key) + 1];
                case "Week":
                    if (string.IsNullOrEmpty(key))
                    {
                        return DateTime.ParseExact(dto.DateFrom.Substring(0, 10), "yyyy/MM/dd", CultureInfo.InvariantCulture).DayOfYear / 7 + "th";
                    }
                    return ((Utility.ConvertToDec(key.Replace("th", "")) + 1) % 52).ToString() + "th";
                case "ALL":
                    return "ALL";

                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private string GetSeries(string type)
        {
            switch (type)
            {
                case "Product":
                    return "KG";

                case "Customer":
                    return "1k-VND";

                default:
                    return "KG";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        private string getTypeStatis(StatisticalDto dto)
        {
            if (string.IsNullOrEmpty(dto.CustomerName))
            {
                return "Product";
            }
            else if (string.IsNullOrEmpty(dto.Product))
            {
                return "Customer";
            }
            else
            {
                return "Product";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        private string getMonth(string num)
        {
            switch (num)
            {
                case "01":
                    return "Jan";
                case "02":
                    return "Feb";
                case "03":
                    return "Mar";
                case "04":
                    return "Apr";
                case "05":
                    return "May";
                case "06":
                    return "Jun";
                case "07":
                    return "Jul";
                case "08":
                    return "Aug";
                case "09":
                    return "Sep";
                case "10":
                    return "Oct";
                case "11":
                    return "Nov";
                case "12":
                    return "Dec";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool checkGrowthChart()
        {
            if (string.IsNullOrEmpty(txtProduct.Text) && string.IsNullOrEmpty(txtCustomerName.Text))
            {
                return false;
            }

            if (uti.GetCustomer().Where(c => string.Equals(c.FullName, txtCustomerName.Text)).Count() != 1 
                && uti.GetProduct().Where(p => string.Equals(p.ProductName, txtProduct.Text)).Count() != 1 )
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Init()
        {
            commandButton1.SetButtonText(new string[8] {
                Utility.CONST.SEARCH_BTN,
                "",
                "",
                "",
                "",
                "",
                "",
                Utility.CONST.CLEAR_BTN,
            });


            ViewModel = new StatisticalDto
            {
                DateFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("yyyy/MM/dd"),
                DateTo = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).ToString("yyyy/MM/dd"),
                Product = string.Empty,
                CustomerName = string.Empty,
                Type = string.Empty
            };

            //txtProduct
            AutoCompleteStringCollection sc = new AutoCompleteStringCollection();

            foreach (var item in uti.GetProduct())
            {
                sc.Add(item.ProductName);
            }

            txtProduct.AutoCompleteCustomSource = sc;

            //txtTel
            sc = new AutoCompleteStringCollection();

            foreach (var item in uti.GetCustomer())
            {
                sc.Add(item.Tel);
            }

            txtCustomerName.AutoCompleteCustomSource = sc;

            //cbType
            BindingList<string> cb = new BindingList<string>();
            cb.Add("ALL");
            cb.Add("Quater");
            cb.Add("Month");
            cb.Add("Week");
            cbType.DataSource = cb;

            ClearDetail();
        }

        /// <summary>
        /// 
        /// </summary>
        private void ClearDetail()
        {
            gDetail.DataSource = new BindingList<GetStaInfoDto>();
            ClearChart(Controls);
        }
        
        private void ChartMouseMove(MouseEventArgs e, Chart chart)
        {
            var pos = e.Location;
            if (prevPosition.HasValue && pos == prevPosition.Value)
                return;
            tooltip.RemoveAll();
            prevPosition = pos;
            var results = chart.HitTest(pos.X, pos.Y, false, ChartElementType.DataPoint);
            foreach (var result in results)
            {
                if (result.ChartElementType == ChartElementType.DataPoint)
                {
                    var prop = result.Object as DataPoint;
                    if (prop != null)
                    {
                        var pointXPixel = result.ChartArea.AxisX.ValueToPixelPosition(prop.XValue);
                        var pointYPixel = result.ChartArea.AxisY.ValueToPixelPosition(prop.YValues[0]);

                        // check if the cursor is really close to the point (2 pixels around the point)

                            tooltip.Show(prop.AxisLabel + ", " + prop.YValues[0], chart,
                                            pos.X, pos.Y - 15);
                        
                    }
                }
            }
        }

        #endregion

        private void chProduct_MouseMove(object sender, MouseEventArgs e)
        {
            ChartMouseMove(e, chProduct);
        }

        private void chCustomer_MouseMove(object sender, MouseEventArgs e)
        {
            ChartMouseMove(e, chCustomer);
        }

        private void chart1_MouseMove(object sender, MouseEventArgs e)
        {
            ChartMouseMove(e, chart1);
        }
    }
}
