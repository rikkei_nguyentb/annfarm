﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class OrderDTO
    {
        public string Tel { get; set; }
        public string FullName { get; set; }
        public string Payed { get; set; }
        public string ShippmentDateFrom { get; set; }
        public string ShippmentDateTo { get; set; }
        public string ShippmentAdd { get; set; }
        public string ShippmentFee { get; set; }
        public string Remark { get; set; }
        public string Total { get; set; }
        public string OrderID { get; set; }
        public BindingList<OrderDetailDTO> LstOrderDetail { get; set; }
    }

    public class OrderDetailDTO
    {
        public string No { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }
        public string Discount { get; set; }
        public string SubTotal { get; set; }
    }
}
