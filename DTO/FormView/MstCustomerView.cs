﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.FormView
{
    public class MstCustomerView
    {
        public string CustomerID { get; set; }
        public string Tel { get; set; }
        public string FullName { get; set; }
        public string Add { get; set; }
        public string Gender { get; set; }
        public string Birthday { get; set; }
        public string DayJoin { get; set; }
        public string Facebook { get; set; }
    }
}
