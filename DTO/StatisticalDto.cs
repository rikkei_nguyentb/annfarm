﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class StatisticalDto
    {
        public string CustomerName { get; set; }
        public string Product { get; set; }
        public string Type { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }

    }

    public class GetStaInfoDto
    {
        public string No { get; set; }
        public string Date { get; set; }
        public string FullName { get; set; }
        public string Product { get; set; }
        public string Quantity { get; set; }
    }
}
