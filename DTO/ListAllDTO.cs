﻿using System.ComponentModel;

namespace DTO
{
    public class ListAllDTO
    {
        public string Tel { get; set; }
        public string FullName { get; set; }
        public string Product { get; set; }
        public string ShipDateFrom { get; set; }
        public string ShipDateTo { get; set; }
        public string Com { get; set; }
        public string Incom { get; set; }
        public BindingList<GridDTO> LstDetail { get; set; }
    }

    public class GridDTO
    {
        private string Telephone { get; set; }

        public string No { get; set; }
        public string ShipDate { get; set; }
        public string Tel {
            get
            {
                return Telephone;
            }
            set
            {
                if (value.Contains(" "))
                {
                    Telephone = value;
                    return;
                }
                if (value.Length > 4)
                {
                    value = value.Insert(4, " ");
                }

                if (value.Length > 8)
                {
                    value = value.Insert(8, " ");
                }

                Telephone = value;
            }
        }
        public string FullName { get; set; }
        public string Product { get; set; }
        public string Quantity { get; set; }
        public string FeeShip { get; set; }
        public string AddDelivery { get; set; }
        public string Total { get; set; }
        public string Payed { get; set; }
        public string Remark { get; set; }
        public string OrderID { get; set; }
    }
}
