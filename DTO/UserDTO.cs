﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class UserDTO
    {
        public string pass { get; set; }
        public string user { get; set; }
        public string tel { get; set; }
    }
}
