﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Collections;

namespace DAO
{
    public class BaseMSAccess : IDisposable
    {
        const string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\SOURCE\MABU.mdb";
        OleDbConnection conn = new OleDbConnection(connectionString);

        public BaseMSAccess()
        {
            conn.Open();
        }

        //public ArrayList QueryToArray(string sql)
        //{

        //}


        #region Dispose context
        /// <summary>
        /// The disposed
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    conn.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
