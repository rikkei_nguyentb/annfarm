﻿using DAO.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ProductRepository : IProductRepository
    {
        ProductTable pro = new ProductTable();

        public void CreateProduct(ProductModel productModel)
        {
            pro.Insert(productModel);
        }

        public string GetNextID()
        {
            var q = (from a in pro.obj
                     select (string)a["ProductID"]).Max();

            if (q == null)
            {
                return "P" + "1".PadLeft(4, '0');
            }           

            return "P" + (int.Parse(q.ToString().Substring(1, q.ToString().Length - 1)) + 1).ToString().PadLeft(4, '0');
        }

        public IEnumerable<ProductModel> GetProduct()
        {
            return (from p in pro.obj
                    select new ProductModel {
                        DateCreate = p["DateCreate"] == null ? string.Empty : p["DateCreate"].ToString(),
                        DateUpdate = p["DateUpdate"] == null ? string.Empty : p["DateUpdate"].ToString(),
                        Price = p["Price"] == null ? string.Empty : p["Price"].ToString(),
                        ProductID = p["ProductID"] == null ? string.Empty : p["ProductID"].ToString(),
                        ProductName = p["ProductName"] == null ? string.Empty : p["ProductName"].ToString(),
                        Source = p["Source"] == null ? string.Empty : p["Source"].ToString(),
                    }).ToList();
        }

        public string GetProductID(string productName)
        {
            return (from a in pro.obj
                    where JToken.DeepEquals(a["ProductName"], new JValue(productName))
                    select (string)a["ProductID"]).FirstOrDefault();
        }

        public IEnumerable<string> GetProductName()
        {
            return (from p in pro.obj
                    select (string)p["ProductName"]).ToList();
        }

        public void UpdateProduct(ProductModel model)
        {

            var oldModel = pro.obj
                .Where(t => string.Equals(t["ProductID"].ToString(), model.ProductID))
                .Select(t => t)
                .SingleOrDefault();

            if (oldModel == null)
            {
                return;
            }

            pro.Update(oldModel.ToObject<ProductModel>(), model);
        }
    }
}
