﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAO
{
    public interface IListAllRepository
    {
        BindingList<GridDTO> GetListAll(string tel, string fullName, string shipDateFrom, string shipDateTo, string product, string chkCom, string chkIncom);
    }
}
