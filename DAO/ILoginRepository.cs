﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAO
{
    public interface ILoginRepository
    {
        Boolean isLogin(string user, string pass);
        int Register(UserDTO obj);
        int Update(UserDTO obj);
    }
}
