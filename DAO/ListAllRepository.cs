﻿using System;
using System.ComponentModel;
using System.Linq;
using DAO.Model;
using DTO;
using Newtonsoft.Json.Linq;

namespace DAO
{
    public class ListAllRepository : IListAllRepository
    {
        private readonly OrderDetailTable orderDetail = new OrderDetailTable();
        private readonly OrderTable order = new OrderTable();
        private readonly ProductTable pro = new ProductTable();
        private readonly CustomerTable cus = new CustomerTable();

        public BindingList<GridDTO> GetListAll(string tel, string fullName, string shipDateFrom, string shipDateTo, string product, string chkCom, string chkIncom)
        {
            var q = (from o in order.obj
                    join od in orderDetail.obj on o["OrderID"] equals od["OrderID"]
                    join p in pro.obj on od["ProductID"] equals p["ProductID"]
                    join c in cus.obj on o["CustomerID"] equals c["CustomerID"]
                    where (string.IsNullOrEmpty(tel) || string.Equals(c["Tel"].ToString(), tel))
                            && (string.IsNullOrEmpty(fullName) || c["FullName"].ToString().ToLower().Contains(fullName.ToLower()))
                            && (string.IsNullOrEmpty(shipDateFrom) || string.Compare(o["ShippmentDateTo"].ToString(), shipDateFrom) >= 0)
                            && (string.IsNullOrEmpty(shipDateTo) || string.Compare(o["ShippmentDateFrom"].ToString(), shipDateTo) <= 0)
                            && (string.IsNullOrEmpty(product) || p["ProductName"].ToString().Contains(product))
                    select new GridDTO
                    {
                        FullName = c["FullName"] == null ? string.Empty : c["FullName"].ToString(),
                        OrderID = o["OrderID"] == null ? string.Empty : o["OrderID"].ToString(),
                        Tel = Utility.CToString(c["Tel"]),
                        Payed = o["Payed"] == null ? string.Empty : o["Payed"].ToString(),
                        Quantity = od["Quantity"] == null ? string.Empty : od["Quantity"].ToString(),
                        Remark = o["Remark"] == null ? string.Empty : o["Remark"].ToString(),
                        Product = p["ProductName"] == null ? string.Empty : p["ProductName"].ToString(),
                        ShipDate = SetShipDate(o["ShippmentDateFrom"], o["ShippmentDateTo"]),
                        FeeShip = o["ShippmentFee"] == null ? string.Empty : o["ShippmentFee"].ToString(),
                        Total = GetSubTotal(od["Quantity"], od["Price"], od["Discount"]),
                        AddDelivery = o["ShippmentAdd"] == null ? string.Empty : o["ShippmentAdd"].ToString(),
                        No = string.Compare(o["ShippmentDateTo"].ToString(), DateTime.Now.ToString("yyyy/MM/dd")) <= 0 || string.Compare(o["Payed"].ToString(), "0") > 0 ? "1" : "3"
                    })
                    .OrderByDescending(i => i.No)
                    .ThenByDescending(i => int.Parse(i.No) < 2 ? i.ShipDate.Substring(15,10) : "0")
                    .ThenBy(i => int.Parse(i.No) > 2 ? i.ShipDate.Substring(15, 10) : "0");

            if (q == null)
            {
                return null;
            }

            return new BindingList<GridDTO>(q.ToList());
        }

        private string GetSubTotal(JToken Quantity, JToken Price, JToken Discount)
        {
            if (Quantity == null || Price == null)
            {
                return string.Empty;
            }

            if (Discount == null)
            {
                return Utility.ConvertToCurrency(Utility.ConvertToDec(Quantity.ToString()) * Utility.ConvertToDec(Price.ToString()));
            }
            else
            {
                return Utility.ConvertToCurrency(Utility.ConvertToDec(Quantity.ToString()) * Utility.ConvertToDec(Price.ToString()) - Utility.ConvertToDec(Discount.ToString()));
            }
            
        }

        private string SetShipDate(JToken ShippmentDateFrom, JToken ShippmentDateTo)
        {
            if (ShippmentDateFrom == null)
            {
                return string.Empty;
            }

            return ShippmentDateFrom.ToString() + "  ~  " + (ShippmentDateTo == null ? string.Empty : ShippmentDateTo.ToString());
        }

    }
}
