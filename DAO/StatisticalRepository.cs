﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.Model;
using DTO;
using Newtonsoft.Json.Linq;

namespace DAO
{
    public class StatisticalRepository : IStatisticalRepository
    {
        private readonly CustomerTable cus = new CustomerTable();
        private readonly ProductTable pro = new ProductTable();
        private readonly OrderDetailTable orderDetail = new OrderDetailTable();
        private readonly OrderTable order = new OrderTable();

        public BindingList<GetStaInfoDto> GetStatisticalDic(StatisticalDto dto)
        {
            int count = 1;
            var q = (from o in order.obj
                     join od in orderDetail.obj on o["OrderID"] equals od["OrderID"]
                     join c in cus.obj on o["CustomerID"] equals c["CustomerID"]
                     join p in pro.obj on od["ProductID"] equals p["ProductID"]
                     where (string.IsNullOrEmpty(dto.CustomerName) || Utility.StringContains(c["FullName"].ToString(), dto.CustomerName))
                         && (string.IsNullOrEmpty(dto.Product) || string.Equals(p["ProductName"].ToString(), dto.Product))
                         && (string.IsNullOrEmpty(dto.DateFrom) || string.Compare(o["ShippmentDateTo"].ToString(), dto.DateFrom) >= 0)
                         && (string.IsNullOrEmpty(dto.DateTo) || string.Compare(o["ShippmentDateFrom"].ToString(), dto.DateTo) <= 0)
                     select new GetStaInfoDto
                     {
                         Date = dto.DateFrom + "  ~  " + dto.DateTo,
                         FullName = Utility.CToString(c["FullName"]),
                         Product = Utility.CToString(p["ProductName"]),
                         Quantity = Utility.CToString(od["Quantity"])
                     })
                     .GroupBy(t => new
                     {
                         t.FullName,
                         t.Product
                     })
                     .OrderByDescending(t => t.Key.FullName).ThenByDescending(t => t.Key.Product)
                     .Select(g => new GetStaInfoDto
                     {
                         No = count++.ToString(),
                         Date = dto.DateFrom + "  ~  " + dto.DateTo,
                         FullName = g.Key.FullName,
                         Product = g.Key.Product,
                         Quantity = Utility.ConvertToCurrency(g.Sum(t => Utility.ConvertToDec(t.Quantity)))
                     });

            if (q == null)
            {
                return null;
            }

            return new BindingList<GetStaInfoDto>(q.ToList());
        }
    }
}
