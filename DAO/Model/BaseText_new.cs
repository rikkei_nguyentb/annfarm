﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace DAO.Model
{
    public class BaseText_new
    {
        private string filePath { get; set; }

        public JArray GetTable(string tableName)
        {
            this.filePath = Environment.CurrentDirectory + "\\DB\\" + tableName + ".json";
            return get();
        }

        public void Commit()
        {
            WriteFile(new JavaScriptSerializer().Serialize(this._obj));
        }

        private string ReadFile()
        {
            return File.ReadAllText(this.filePath);
        }

        private void WriteFile(string values)
        {
            File.WriteAllText(this.filePath, values);
        }

        private JArray get()
        {
            string json = ReadFile();

            if (string.IsNullOrEmpty(json))
            {
                json = "[]";
            }

            return JArray.Parse(json);
        }
    }
}
