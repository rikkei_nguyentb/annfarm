﻿namespace DAO.Model
{
    public class OrderModel
    {
        public string CustomerID { get; set; }
        public string ShippmentDateFrom { get; set; }
        public string ShippmentDateTo { get; set; }
        public string ShippmentAdd { get; set; }
        public string ShippmentFee { get; set; }
        public string Payed { get; set; }
        public string Remark { get; set; }
        public string OrderID { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
        public string DestFlag { get; set; }
    }

    public class OrderDetailModel
    {
        public string OrderID { get; set; }
        public string ProductID { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }
        public string Discount { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
    }

    public class ProductModel
    {
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string Price { get; set; }
        public string Source { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
        public string DestFlag { get; set; }
    }

    public class DiscountModel
    {
        public string ProductID { get; set; }
        public string Quantity { get; set; }
        public string Discount { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
    }

    public class CustomerModel
    {
        public string CustomerID { get; set; }
        public string Tel { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string FaceBook { get; set; }
        public string Address { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
        public string DestFlag { get; set; }
    }

    public class UserModel
    {
        public string pass { get; set; }
        public string user { get; set; }
        public string tel { get; set; }
    }

    public class UserTable : BaseText<UserModel> { public UserTable() : base("User") { } }
    public class OrderTable : BaseText<OrderModel> { public OrderTable() : base("Order") { } }
    public class OrderDetailTable : BaseText<OrderDetailModel> { public OrderDetailTable() : base("OrderDetail") { } }
    public class CustomerTable : BaseText<CustomerModel> { public CustomerTable() : base("Customer") { } }
    public class ProductTable : BaseText<ProductModel> { public ProductTable() : base("Product") { } }
    public class DiscountTable : BaseText<DiscountModel> { public DiscountTable() : base("Discount") { } }
}
