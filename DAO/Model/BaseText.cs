﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DAO
{
    public class BaseText<T> where T : class
    {
        private static bool isOpenTransaction = false;

        private readonly string folderPath = Environment.CurrentDirectory + "\\DB\\";
        private readonly string bkFolder = Environment.CurrentDirectory + "\\DB_BK\\";

        private string filePath;
        private JArray _obj;

        public JArray obj
        {
            get
            {
                _obj = get();
                return _obj;
            }
        }

        public BaseText(string tableName)
        {
            filePath = Environment.CurrentDirectory + "\\DB\\" + tableName + ".json";
            Init();
            BackupData();
        }

        //public List<T> get(Dictionary<string, string> condition)
        //{
        //    List<T> lstObj = get();
        //    List<T> result = new List<T>();
        //    bool isValid;

        //    foreach (T obj in lstObj)
        //    {
        //        isValid = true;

        //        foreach (string key in condition.Keys)
        //        {
        //            foreach (PropertyInfo p in typeof(T).GetProperties())
        //            {
        //                if (key.Equals(p.Name) && !condition[key].Equals(p.GetValue(obj).ToString()))
        //                {
        //                    isValid = false;
        //                    break;
        //                }
        //            }
        //        }

        //        if (isValid)
        //        {
        //            result.Add(obj);
        //        }
        //    }

        //    return result;
        //}

        public int Delete(T obj)
        {
            try
            {
                _obj = this.obj;
                _obj.Remove(JObject.FromObject(obj));
                Commit();

                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public int Delete(JToken obj)
        {
            try
            {
                _obj = this.obj;

                _obj.Where(t => CompareT(t.ToObject<T>(), obj.ToObject<T>()))
                        .Select(t => t)
                        .FirstOrDefault()
                        .Remove();

                Commit();

                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public int Delete(List<JToken> lstObj)
        {
            int count = 0;

            try
            {
                foreach (JToken o in lstObj)
                {
                    _obj.Where(t => CompareT(t.ToObject<T>(), o.ToObject<T>()))
                        .Select(t => t)
                        .FirstOrDefault()
                        .Remove();

                    count++;
                }

                Commit();
                return count;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        /// <summary>
        /// Insert obj
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int Insert(T obj)
        {
            try
            {
                _obj.Add(JObject.FromObject(obj));
                Commit();

                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// update old object to new object
        /// </summary>
        /// <param name="oldObj"></param>
        /// <param name="newObj"></param>
        /// <returns></returns>
        public int Update(T oldObj, T newObj)
        {
            int count = 0;

            try
            {
                foreach (var item in _obj.Where(t => CompareT(oldObj, t.ToObject<T>())))
                {
                    foreach (PropertyInfo prop in typeof(T).GetProperties())
                    {
                        if (string.Equals(prop.Name, "DateCreate"))
                        {
                            continue;
                        }

                        if (string.Equals(prop.Name, "DateUpdate"))
                        {
                            item[prop.Name] = DateTime.Now.ToString("yyyyMMdd");
                            continue;
                        }

                        PropertyInfo t2 = newObj.GetType().GetProperty(prop.Name);

                        if (t2.GetValue(newObj) == null)
                        {
                            continue;
                        }

                        item[prop.Name] = t2.GetValue(newObj).ToString();
                    }

                    count++;
                }

                Commit();

                return count;
            }
            catch
            {
                return 0;
            }
        }

        public void BeginTransaction()
        {
            isOpenTransaction = true;
        }

        public void RollbackTransaction()
        {
            //do nothing =))
        }

        public void CommitTransaction()
        {
            isOpenTransaction = false;
            Commit();
        }

        #region Private Function
        private JArray get()
        {
            string json = ReadFile();

            if (string.IsNullOrEmpty(json))
            {
                json = "[]";
            }

            JArray res = JArray.Parse(json);

            res = new JArray(res.GroupBy(t => t.ToString())
                    .Select(t => t.First())); 

            return res;
        }

        private void Init()
        {
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            if (!File.Exists(filePath))
            {
                var myFile = File.Create(filePath);
                myFile.Close();
            }

            //_obj = get();
        }

        private string ReadFile()
        {
            return File.ReadAllText(filePath);
        }

        private void WriteFile(string values)
        {
            string fileBK = filePath + ".bk";

            try
            {
                File.Copy(filePath, fileBK, true);
                File.WriteAllText(filePath, values);
                File.Delete(fileBK);
            }
            catch
            {
                File.Copy(fileBK, filePath, true);
            }
        }

        private void BackupData()
        {
            if (!Directory.Exists(bkFolder))
            {
                Directory.CreateDirectory(bkFolder);
            }

            string yesterdayBkFd = bkFolder + DateTime.Now.AddDays(-1).ToString("yyyyMMdd") + "\\";
            string todayBkFd = bkFolder + DateTime.Now.ToString("yyyyMMdd") + "\\";

            //check nearly bk folder is created
            if (!(Directory.Exists(yesterdayBkFd) || Directory.Exists(todayBkFd)))
            {
                //create new bk
                Directory.CreateDirectory(todayBkFd);
                Copy(folderPath, todayBkFd);
            }

            //delete old bk
            int numOfBkFd = Directory.GetDirectories(bkFolder, "*", SearchOption.TopDirectoryOnly).Length;
            while (numOfBkFd > 2)
            {
                string deleteFolder = Directory.GetDirectories(bkFolder, "*", SearchOption.TopDirectoryOnly).OrderBy(i => i).ToArray()[0];

                Directory.Delete(deleteFolder, true);

                numOfBkFd = Directory.GetDirectories(bkFolder, "*", SearchOption.TopDirectoryOnly).Length;
            }
        }

        private bool CompareT(T o1, T o2)
        {
            foreach (PropertyInfo prop in typeof(T).GetProperties())
            {
                PropertyInfo t1 = o1.GetType().GetProperty(prop.Name);
                PropertyInfo t2 = o2.GetType().GetProperty(prop.Name);

                if (!Equals(t1.GetValue(o1), t2.GetValue(o2)))
                {
                    return false;
                }
            }

            return true;
        }

        private void Commit()
        {
            if (isOpenTransaction)
            {
                return;
            }

            //WriteFile(this._obj.ToString(Formatting.None));
            WriteFile(_obj.ToString());
        }

        private void Copy(string SourcePath, string DestinationPath)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath), true);
        }
        #endregion Private Function
    }
}
