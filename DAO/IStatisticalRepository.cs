﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAO
{
    public interface IStatisticalRepository
    {
        BindingList<GetStaInfoDto> GetStatisticalDic(StatisticalDto dto);
    }
}
