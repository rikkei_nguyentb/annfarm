﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.Model;
using DTO;

namespace DAO
{
    public interface IOrderRepository
    {
        OrderDTO GetOrder(string tel, string shipmentDateF, string shipmentDateT);
        int InsertOrderModel(OrderModel orderModel);
        int UpdateOrder(OrderModel orderModel);
        int InsertOrderDetailModel(OrderDetailModel orderDetailModel);
        void DeleteOrderDetail(string orderID);
        OrderDTO GetOrder(string orderID);
        int DeleteOrder(string orderID);
    }
}
