﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.Model;
using DTO;
using Newtonsoft.Json.Linq;

namespace DAO
{
    public class OrderRepository : IOrderRepository
    {
        OrderTable order = new OrderTable();
        OrderDetailTable orderDetail = new OrderDetailTable();
        CustomerTable cus = new CustomerTable();
        ProductTable pro = new ProductTable();

        public int DeleteOrder(string orderID)
        {
            var q = order.obj
                    .Where(t => string.Equals(t["OrderID"].ToString(), orderID))
                    .Select(t => t)
                    .SingleOrDefault();

            if (q == null)
            {
                return 0;
            }

            return order.Delete(q);
        }

        public void DeleteOrderDetail(string orderID)
        {
            var q = orderDetail.obj
                        .Where(t => string.Equals(t["OrderID"].ToString(), orderID))
                        .Select(t => t)
                        .ToList();

            if (q.Count() == 0)
            {
                return;
            }

            orderDetail.Delete(q);
        }

        public OrderDTO GetOrder(string tel, string shippmentDateF, string shippmentDateT)
        {
            OrderDTO res = (from o in order.obj
                            join od in orderDetail.obj on o["OrderID"] equals od["OrderID"]
                            join c in cus.obj on o["CustomerID"] equals c["CustomerID"]
                            join p in pro.obj on od["ProductID"] equals p["ProductID"]
                            where JToken.DeepEquals(c["Tel"], new JValue(tel))
                                && JToken.DeepEquals(o["ShippmentDateFrom"], new JValue(shippmentDateF))
                                && JToken.DeepEquals(o["ShippmentDateTo"], new JValue(shippmentDateT))
                            select new OrderDTO
                            {
                                FullName = Utility.CToString(c["FullName"]),
                                Payed = Utility.CToString(o["Payed"]),
                                ShippmentAdd = Utility.CToString(o["ShippmentAdd"]),
                                ShippmentDateFrom = Utility.CToString(o["ShippmentDateFrom"]),
                                ShippmentDateTo = Utility.CToString(o["ShippmentDateTo"]),
                                ShippmentFee = Utility.CToString(o["ShippmentFee"]),
                                Tel = Utility.CToString(c["Tel"]),
                                Remark = Utility.CToString(o["Remark"]),
                                OrderID = Utility.CToString(o["OrderID"])
                            }).FirstOrDefault();

            if (res == null)
            {
                return null;
            }

            List<OrderDetailDTO> lst = (from od in orderDetail.obj
                                        join p in pro.obj on od["ProductID"] equals p["ProductID"]
                                        where JToken.DeepEquals(od["OrderID"], new JValue(res.OrderID))
                                        select new OrderDetailDTO
                                        {
                                            Discount = od["Discount"] == null ? string.Empty : od["Discount"].ToString(),
                                            Price = od["Price"] == null ? string.Empty : od["Price"].ToString(),
                                            ProductID = od["ProductID"] == null ? string.Empty : od["ProductID"].ToString(),
                                            ProductName = p["ProductName"] == null ? string.Empty : p["ProductName"].ToString(),
                                            Quantity = od["Quantity"] == null ? string.Empty : od["Quantity"].ToString()
                                        }).ToList();
            res.LstOrderDetail = new BindingList<OrderDetailDTO>(lst);

            return res;
        }

        public OrderDTO GetOrder(string orderID)
        {
            int count = 0;

            var q = order.obj
                    .GroupJoin(orderDetail.obj,
                        t1 => t1["OrderID"].ToString(),
                        t2 => t2["OrderID"].ToString(),
                        (t1, t2) => new { order = t1, orderdetail = t2 })
                    .Join(cus.obj,
                        t1 => t1.order["CustomerID"].ToString(),
                        t2 => t2["CustomerID"].ToString(),
                        (t1, t2) => new { t1.order, t1.orderdetail, customer = t2 })
                    .Where(t => string.Equals(t.order["OrderID"].ToString(), orderID))
                    .Select(t => new OrderDTO
                    {
                        FullName = Utility.CToString(t.customer["FullName"]),
                        OrderID = Utility.CToString(t.order["OrderID"]),
                        Payed = Utility.CToString(t.order["Payed"]),
                        Remark = Utility.CToString(t.order["Remark"]),
                        ShippmentAdd = Utility.CToString(t.order["ShippmentAdd"]),
                        ShippmentDateFrom = Utility.CToString(t.order["ShippmentDateFrom"]),
                        ShippmentDateTo = Utility.CToString(t.order["ShippmentDateTo"]),
                        ShippmentFee = Utility.CToString(t.order["ShippmentFee"]),
                        Tel = Utility.CToString(t.customer["Tel"]),
                        Total = Utility.ConvertToCurrency(t.orderdetail.Select(a => Utility.ConvertToDec(a["Quantity"]) * Utility.ConvertToDec(a["Price"]) - Utility.ConvertToDec(a["Discount"]))
                                                                        .Sum() + Utility.ConvertToDec(t.order["ShippmentFee"])),
                        LstOrderDetail = new BindingList<OrderDetailDTO>(t.orderdetail.Select(x => new OrderDetailDTO
                        {
                            Discount = Utility.CToString(x["Discount"]),
                            Price = Utility.CToString(x["Price"]),
                            ProductID = Utility.CToString(x["ProductID"]),
                            ProductName = pro.obj.Where(a => Equals(x["ProductID"], a["ProductID"]))
                                                    .Select(a => a["ProductName"])
                                                    .FirstOrDefault()
                                                    .ToString(),
                            Quantity = Utility.CToString(x["Quantity"]),
                            SubTotal = Utility.ConvertToCurrency(Utility.ConvertToDec(x["Quantity"]) * Utility.ConvertToDec(x["Price"]) - Utility.ConvertToDec(x["Discount"])),
                            No = (++count).ToString()
                        }).ToList())
                    })
                    .FirstOrDefault();

            return q;
        }

        public int InsertOrderDetailModel(OrderDetailModel orderDetailModel)
        {
            return orderDetail.Insert(orderDetailModel);
        }

        public int InsertOrderModel(OrderModel orderModel)
        {
            return order.Insert(orderModel);
        }

        public int UpdateOrder(OrderModel orderModel)
        {
            var oldModel = order.obj
                .Where(t => string.Equals(t["OrderID"].ToString(), orderModel.OrderID))
                .Select(t => t)
                .SingleOrDefault();

            if (oldModel == null)
            {
                return 0;
            }

            return order.Update(oldModel.ToObject<OrderModel>(), orderModel);
        }

        #region comment update
        public int UpdateOrderDetail(OrderDetailModel orderDetailModel)
        {
            var oldModel = orderDetail.obj
                .Where(t => string.Equals(t["OrderID"].ToString(), orderDetailModel.OrderID))
                .Select(t => t)
                .SingleOrDefault();

            if (oldModel == null)
            {
                return 0;
            }

            return orderDetail.Update(oldModel.ToObject<OrderDetailModel>(), orderDetailModel);
        }
        #endregion

    }
}
