﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.Model;
using Newtonsoft.Json.Linq;

namespace DAO
{
    public class CustomerRepository : ICustomerRepository
    {
        CustomerTable cus = new CustomerTable();

        public void Create(CustomerModel customerModel)
        {
            cus.Insert(customerModel);
        }

        public string GetNextID()
        {
            var q = (from a in cus.obj
                     select (string)a["CustomerID"]).Max();

            if (q == null)
            {
                return "C" + "1".PadLeft(8, '0');
            }

            return "C" + (int.Parse(q.ToString().Substring(1, q.ToString().Length - 1)) + 1).ToString().PadLeft(8, '0');
        }

        public CustomerModel GetCustomer(string tel)
        {
            return (from c in cus.obj
                    where string.Equals(c["Tel"].ToString(), tel)
                    select new CustomerModel
                    {
                        Address = c["Address"] == null ? string.Empty : c["Address"].ToString(),
                        DateCreate = c["DateCreate"] == null ? string.Empty : c["DateCreate"].ToString(),
                        DateUpdate = c["DateUpdate"] == null ? string.Empty : c["DateUpdate"].ToString(),
                        FaceBook = c["FaceBook"] == null ? string.Empty : c["FaceBook"].ToString(),
                        FullName = c["FullName"] == null ? string.Empty : c["FullName"].ToString(),
                        Gender = c["Gender"] == null ? string.Empty : c["Gender"].ToString(),
                        Tel = c["Tel"] == null ? string.Empty : c["Tel"].ToString(),
                        CustomerID = Utility.CToString(c["CustomerID"])
                    }).SingleOrDefault();
        }

        public IEnumerable<CustomerModel> GetCustomer()
        {
            return (from c in cus.obj
                    select new CustomerModel
                    {
                        Address = c["Address"] == null ? string.Empty : c["Address"].ToString(),
                        DateCreate = c["DateCreate"] == null ? string.Empty : c["DateCreate"].ToString(),
                        DateUpdate = c["DateUpdate"] == null ? string.Empty : c["DateUpdate"].ToString(),
                        FaceBook = c["FaceBook"] == null ? string.Empty : c["FaceBook"].ToString(),
                        FullName = c["FullName"] == null ? string.Empty : c["FullName"].ToString(),
                        Gender = c["Gender"] == null ? string.Empty : c["Gender"].ToString(),
                        Tel = c["Tel"] == null ? string.Empty : c["Tel"].ToString(),
                        CustomerID = Utility.CToString(c["CustomerID"])
                    }).ToList();
        }

        public bool isExits(CustomerModel customerModel)
        {
            var q = (from a in cus.obj
                     where JToken.DeepEquals(a["Tel"], new JValue(customerModel.Tel))
                     select a).Count();

            return q != 0;
        }

        public int Update(CustomerModel customerModel)
        {
            var oldModel = cus.obj
                .Where(t => string.Equals(t["CustomerID"].ToString(), customerModel.CustomerID)
                                && !string.Equals(t["DestFlag"].ToString(), "1"))
                .Select(t => t)
                .SingleOrDefault();

            if (oldModel == null)
            {
                return 0;
            }

            return cus.Update(oldModel.ToObject<CustomerModel>(), customerModel);
        }

        public string GetCustomerID(string tel)
        {
            return cus.obj
                .Where(x => string.Equals(x["Tel"].ToString(), tel))
                .Select(x => (string)x["CustomerID"])
                .SingleOrDefault();
        }
    }
}
