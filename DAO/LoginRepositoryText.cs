﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.Model;
using DTO;
using Newtonsoft.Json.Linq;

namespace DAO
{
    public class LoginRepositoryText : ILoginRepository
    {
        private UserTable userTable = new UserTable();

        public bool isLogin(string user, string pass)
        {
            return (from t in userTable.obj
                    where JToken.DeepEquals(t["user"], new JValue(user)) 
                        && JToken.DeepEquals(t["pass"], new JValue(pass))
                     select new { }).Count() == 1;
        }

        public int Register(UserDTO obj)
        {
            return userTable.Insert(new UserModel {
                tel = obj.tel,
                pass = obj.pass,
                user = obj.user
            });
        }

        public int Update(UserDTO obj)
        {
            foreach (var item in userTable.obj.Where(t => JToken.DeepEquals(t["user"], new JValue("nguyen"))))
            {
                item["pass"] = "";
            }

            //userTable.Commit();
            return 1;
        }


    }


}
