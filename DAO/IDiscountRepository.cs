﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.Model;

namespace DAO
{
    public interface IDiscountRepository
    {
        bool Exits(DiscountModel model);
        void Update(DiscountModel model);
        void Insert(DiscountModel model);
        DiscountModel GetDiscount(string productID, string quantity);
    }
}
