﻿using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace DAO
{
    public static class Utility
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertToCurrency(object value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            return string.Format("{0:0,0}", Convert.ToDecimal(value));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static bool StringContains(string s1, string s2)
        {
            if (s2 == null)
            {
                return true;
            }
            return ConvertToUnSign(s1.ToLower()).Contains(ConvertToUnSign(s2.ToLower()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string ConvertToUnSign(string input)
        {
            input = input.Trim();
            for (int i = 0x20; i < 0x30; i++)
            {
                input = input.Replace(((char)i).ToString(), " ");
            }
            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string str = input.Normalize(NormalizationForm.FormD);
            string str2 = regex.Replace(str, string.Empty).Replace('đ', 'd').Replace('Đ', 'D');
            while (str2.IndexOf("?") >= 0)
            {
                str2 = str2.Remove(str2.IndexOf("?"), 1);
            }
            return str2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNumeric(object value)
        {
            if (value == null)
            {
                return true;
            }

            return decimal.TryParse(value.ToString(), System.Globalization.NumberStyles.Currency, CultureInfo.InvariantCulture, out decimal result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal ConvertToDec(object value)
        {
            decimal result;

            if (value == null)
            {
                return 0;
            }

            decimal.TryParse(value.ToString(), NumberStyles.Currency, CultureInfo.InvariantCulture, out result);

            return result;
        }

        public static string CToString(object value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            return value.ToString();
        }

        public static int NumCompare(object o1, object o2)
        {
            if (o1 == null)
            {
                return -1;
            }

            if (o2 == null)
            {
                return 1;
            }

            if (ConvertToDec(o1.ToString()) - ConvertToDec(o2.ToString()) < 0)
            {
                return -1;
            }
            else if(ConvertToDec(o1.ToString()) - ConvertToDec(o2.ToString()) == 0)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
    }
}
