﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.Model;

namespace DAO
{
    public interface ICustomerRepository
    {
        void Create(CustomerModel customerModel);
        bool isExits(CustomerModel customerModel);
        CustomerModel GetCustomer(string tel);
        int Update(CustomerModel customerModel);
        IEnumerable<CustomerModel> GetCustomer();
        string GetNextID();
        string GetCustomerID(string tel);
    }
}
