﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.Model;
using Newtonsoft.Json.Linq;

namespace DAO
{
    public class DiscountRepository : IDiscountRepository
    {
        private DiscountTable discount = new DiscountTable();

        public bool Exits(DiscountModel model)
        {
            return discount.obj
                .Where(t => string.Equals(t["ProductID"].ToString(), model.ProductID)
                            && string.Equals(t["Quantity"].ToString(), model.Quantity))
                .Select(t => t)
                .ToList()
                .Count() == 1;
        }

        public DiscountModel GetDiscount(string productID, string quantity)
        {
            return (from d in discount.obj
                    where string.Equals(productID, d["ProductID"].ToString())
                            && Utility.NumCompare(quantity, d["Quantity"]) >= 0
                    orderby d["Quantity"] descending
                    select new DiscountModel
                    {
                        DateCreate = Utility.CToString(d["DateCreate"]),
                        DateUpdate = Utility.CToString(d["DateUpdate"]),
                        Discount = Utility.CToString(d["Discount"]),
                        ProductID = Utility.CToString(d["ProductID"]),
                        Quantity = Utility.CToString(d["Quantity"])
                    }).FirstOrDefault();
        }

        public void Insert(DiscountModel model)
        {
            discount.Insert(model);
        }

        public void Update(DiscountModel model)
        {
            var oldModel = discount.obj
                .Where(t => string.Equals(t["ProductID"].ToString(), model.ProductID)
                            && string.Equals(t["Quantity"].ToString(), model.Quantity))
                .Select(t => t)
                .SingleOrDefault();

            if (oldModel == null)
            {
                return;
            }

            discount.Update(oldModel.ToObject<DiscountModel>(), model);
        }
    }
}
