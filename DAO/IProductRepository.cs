﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.Model;

namespace DAO
{
    public interface IProductRepository
    {
        string GetProductID(string productName);
        void CreateProduct(Model.ProductModel productModel);
        string GetNextID();
        IEnumerable<string> GetProductName();
        IEnumerable<ProductModel> GetProduct();
        void UpdateProduct(ProductModel model);
    }
}
